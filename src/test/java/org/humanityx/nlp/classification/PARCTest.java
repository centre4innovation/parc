package org.humanityx.nlp.classification;

import org.humanityx.nlp.classification.db.ClassificationDAO;
import org.humanityx.nlp.classification.db.DAO;
import org.humanityx.nlp.classification.pojo.ClassificationScores;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by Wouter Eekhout on 22/11/2017.
 */
public class PARCTest {
    PARC parc;

    @Test
    public void exampleRunTest() throws Exception {
        NLPEnglish nlp = new NLPEnglish();
        parc = new PARC("data/db/test_database.sqlite");

        createSDGs(parc);

        parc.init();
        train(parc, nlp); //Train parc using the 17 Sustainable Development Goals

        String input = "Habjouqa Despite advances towards gender parity in primary schools in many countries significant gender gaps remain";
        ClassificationScores scores = parc.predict(nlp, input, 0.3, 0.01, 0.1);

        System.out.println("Classifications: " + scores.getPredictions()); //expected 4 and 5

        Assert.assertEquals(scores.getPredictions().size(), 2);
        Assert.assertTrue(scores.getPredictions().contains((long)4));
        Assert.assertTrue(scores.getPredictions().contains((long)5));
    }

    private void createSDGs(PARC parc) throws SQLException {
        DAO dao = parc.getDao();
        ClassificationDAO classificationDAO = dao.getClassificationDAO();
        System.out.println("Creating classifications");

        classificationDAO.insert("1: No Poverty");
        classificationDAO.insert("2: Zero Hunger");
        classificationDAO.insert("3: Good Health and Well-being");
        classificationDAO.insert("4: Quality Education");
        classificationDAO.insert("5: Gender Equality");
        classificationDAO.insert("6: Clean Water and Sanitation");
        classificationDAO.insert("7: Affordable and Clean Energy");
        classificationDAO.insert("8: Decent Work and Economic Growth");
        classificationDAO.insert("9: Industry, Innovation and Infrastructure");
        classificationDAO.insert("10: Reduced Inequality");
        classificationDAO.insert("11: Sustainable Cities and Communities");
        classificationDAO.insert("12: Responsible Consumption and Production");
        classificationDAO.insert("13: Climate Action");
        classificationDAO.insert("14: Life Below Water");
        classificationDAO.insert("15: Life on Land");
        classificationDAO.insert("16: Peace and Justice Strong Institutions");
        classificationDAO.insert("17: Partnerships to achieve the Goal");
    }

    private void train(PARC parc, INLP nlp) throws SQLException, InterruptedException {
        System.out.println("Train SDG 1");
        parc.train(nlp, TestData.sustainableDevelopmentGoal1, 1);
        System.out.println("Train SDG 2");
        parc.train(nlp, TestData.sustainableDevelopmentGoal2, 2);
        System.out.println("Train SDG 3");
        parc.train(nlp, TestData.sustainableDevelopmentGoal3, 3);
        System.out.println("Train SDG 4");
        parc.train(nlp, TestData.sustainableDevelopmentGoal4, 4);
        System.out.println("Train SDG 5");
        parc.train(nlp, TestData.sustainableDevelopmentGoal5, 5);
        System.out.println("Train SDG 6");
        parc.train(nlp, TestData.sustainableDevelopmentGoal6, 6);
        System.out.println("Train SDG 7");
        parc.train(nlp, TestData.sustainableDevelopmentGoal7, 7);
        System.out.println("Train SDG 8");
        parc.train(nlp, TestData.sustainableDevelopmentGoal8, 8);
        System.out.println("Train SDG 9");
        parc.train(nlp, TestData.sustainableDevelopmentGoal9, 9);
        System.out.println("Train SDG 10");
        parc.train(nlp, TestData.sustainableDevelopmentGoal10, 10);
        System.out.println("Train SDG 11");
        parc.train(nlp, TestData.sustainableDevelopmentGoal11, 11);
        System.out.println("Train SDG 12");
        parc.train(nlp, TestData.sustainableDevelopmentGoal12, 12);
        System.out.println("Train SDG 13");
        parc.train(nlp, TestData.sustainableDevelopmentGoal13, 13);
        System.out.println("Train SDG 14");
        parc.train(nlp, TestData.sustainableDevelopmentGoal14, 14);
        System.out.println("Train SDG 15");
        parc.train(nlp, TestData.sustainableDevelopmentGoal15, 15);
        System.out.println("Train SDG 16");
        parc.train(nlp, TestData.sustainableDevelopmentGoal16, 16);
        System.out.println("Train SDG 17");
        parc.train(nlp, TestData.sustainableDevelopmentGoal17, 17);
    }

    @After
    public void tearDown() throws SQLException {
        DAO dao = parc.getDao();

        dao.getClassificationDAO().drop();
        dao.getClassificationTokenDAO().drop();
    }

}
