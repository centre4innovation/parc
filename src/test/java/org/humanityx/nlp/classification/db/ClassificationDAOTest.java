package org.humanityx.nlp.classification.db;

import org.humanityx.nlp.classification.pojo.Classification;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class ClassificationDAOTest {
    private Connect connect;
    private ClassificationDAO classificationDAO;

    @Before
    public void initDAO() throws SQLException {
        connect = new Connect("data/db/test_database.sqlite");
        classificationDAO = new ClassificationDAO(connect);

        classificationDAO.drop();
        classificationDAO.ensureTable();
    }

    @Test
    public void insertTest() throws SQLException {
        int id = classificationDAO.insert("insertTest");

        assert(id > 0);
    }

    @Test
    public void getTest() throws SQLException {
        int id = classificationDAO.insert("getTest");

        Classification classification = classificationDAO.get(id);

        Assert.assertEquals("getTest", classification.getName());
        Assert.assertEquals(0, classification.getTotalTokenFreq());
    }

    @Test
    public void updateTest() throws SQLException {
        int id = classificationDAO.insert("updateTest");

        classificationDAO.update(id, "updateTest2", 10);

        Classification classification = classificationDAO.get(id);

        Assert.assertEquals("updateTest2", classification.getName());
        Assert.assertEquals(10, classification.getTotalTokenFreq());
    }

    @Test
    public void deleteTest() throws SQLException {
        int id = classificationDAO.insert("deleteTest");

        classificationDAO.delete(id);

        try {
            classificationDAO.get(id);
        } catch (SQLException ex) {
            Assert.assertEquals("Get failed", ex.getMessage());
            return;
        }

        Assert.fail(); //the get function should not succeed
    }

    @Test
    public void getAllTest() throws SQLException {
        classificationDAO.insert("getAllTest1");
        classificationDAO.insert("getAllTest2");

        List<Classification> list = classificationDAO.get();

        Assert.assertEquals(2, list.size());
    }

    @Test
    public void addFrequencyTest() throws  SQLException {
        int id = classificationDAO.insert("addFrequencyTest");

        classificationDAO.addFrequency(id, 1);
        classificationDAO.addFrequency(id, 5);

        Classification classification = classificationDAO.get(id);

        Assert.assertEquals(6, classification.getTotalTokenFreq());
    }


    @After
    public void tearDown() throws SQLException {
        connect.getConnection().close();
    }
}
