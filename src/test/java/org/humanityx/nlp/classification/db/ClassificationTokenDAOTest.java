package org.humanityx.nlp.classification.db;

import org.humanityx.nlp.classification.pojo.ClassificationToken;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class ClassificationTokenDAOTest {
    private Connect connect;
    private ClassificationTokenDAO classificationTokenDAO;

    @Before
    public void initDAO() throws SQLException {
        connect = new Connect("data/db/test_database.sqlite");
        classificationTokenDAO = new ClassificationTokenDAO(connect);

        classificationTokenDAO.drop();
        classificationTokenDAO.ensureTable();
    }

    @Test
    public void insertTest() throws SQLException {
        int id = classificationTokenDAO.insert(1, "insertTest", "NN");

        assert(id > 0);
    }

    @Test
    public void existsTest() throws SQLException {
        boolean exists = classificationTokenDAO.exists(1, "existsTest", "NN");

        Assert.assertEquals(false, exists);

        classificationTokenDAO.insert(1, "existsTest", "NN");

        exists = classificationTokenDAO.exists(1, "existsTest", "NN");

        Assert.assertEquals(true, exists);
    }

    @Test
    public void getTest() throws SQLException {
        int id = classificationTokenDAO.insert(1, "getTest", "NN");

        ClassificationToken classificationToken = classificationTokenDAO.get(1,"getTest", "NN");

        Assert.assertEquals(1, classificationToken.getClassification());
        Assert.assertEquals("getTest", classificationToken.getToken());
        Assert.assertEquals("NN", classificationToken.getPos());
        Assert.assertEquals(id, classificationToken.getId());
        Assert.assertEquals(0, classificationToken.getFrequency());
    }

    @Test
    public void deleteByClassificationTest() throws SQLException {
        classificationTokenDAO.insert(1, "deleteByClassificationTest1", "NN");
        classificationTokenDAO.insert(1, "deleteByClassificationTest2", "NN");
        classificationTokenDAO.insert(2, "deleteByClassificationTest3", "NN");

        classificationTokenDAO.deleteByClassification(1);

        Assert.assertEquals(false, classificationTokenDAO.exists(1, "deleteByClassificationTest1", "NN"));
        Assert.assertEquals(false, classificationTokenDAO.exists(1, "deleteByClassificationTest2", "NN"));
        Assert.assertEquals(true, classificationTokenDAO.exists(2, "deleteByClassificationTest3", "NN"));
    }

    @Test
    public void countTokenInClassificationTest() throws SQLException {
        classificationTokenDAO.insert(1, "countTokenInClassificationTest", "NN");
        classificationTokenDAO.insert(2, "countTokenInClassificationTest", "NN");
        classificationTokenDAO.insert(3, "countTokenInClassificationTest1", "NN"); //different word

        int count = classificationTokenDAO.countTokenInClassification("countTokenInClassificationTest" , "NN");

        Assert.assertEquals(2, count);
    }

    @Test
    public void getTokenFrequencyInClassificationTest() throws SQLException {
        classificationTokenDAO.insert(1, "countTokenInClassificationTest", "NN");
        classificationTokenDAO.addFrequency(1, "countTokenInClassificationTest", "NN", 3);

        classificationTokenDAO.insert(2, "countTokenInClassificationTest", "NN");
        classificationTokenDAO.addFrequency(2, "countTokenInClassificationTest", "NN", 8);

        classificationTokenDAO.insert(2, "countTokenInClassificationTest1", "NN"); //different word
        classificationTokenDAO.addFrequency(2, "countTokenInClassificationTest1", "NN", 7);

        int count1 = classificationTokenDAO.getTokenFrequencyInClassification(1,
                "countTokenInClassificationTest", "NN");
        int count2 = classificationTokenDAO.getTokenFrequencyInClassification(2,
                "countTokenInClassificationTest", "NN");
        int count3 = classificationTokenDAO.getTokenFrequencyInClassification(2,
                "countTokenInClassificationTest1", "NN");

        Assert.assertEquals(3, count1);
        Assert.assertEquals(8, count2);
        Assert.assertEquals(7, count3);

        //not existing test
        try {
            int count4 = classificationTokenDAO.getTokenFrequencyInClassification(2,
                    "countTokenInClassificationTest2", "NN");
        } catch (SQLException ex) {
            Assert.assertEquals("No result", ex.getMessage());
            return;
        }

        Assert.fail();//the count4 test should fail
    }

    @Test
    public void addFrequencyTest() throws SQLException {
        classificationTokenDAO.insert(1, "addFrequencyTest", "NN");

        classificationTokenDAO.addFrequency(1, "addFrequencyTest", "NN", 1);
        classificationTokenDAO.addFrequency(1, "addFrequencyTest", "NN", 5);

        ClassificationToken classificationToken = classificationTokenDAO.get(1,"addFrequencyTest", "NN");

        Assert.assertEquals(6, classificationToken.getFrequency());
    }

    @After
    public void tearDown() throws SQLException {
        connect.getConnection().close();
    }
}
