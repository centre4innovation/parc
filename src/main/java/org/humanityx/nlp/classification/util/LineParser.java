package org.humanityx.nlp.classification.util;

import java.io.*;
import java.util.function.Consumer;

/**
 * Iterate and process each line in a (list of) File(s)
 * @author Arvid Halma
 * @version 13-10-2015 - 21:07
 */
public class LineParser {

    public static void lines(Consumer<String> lineConsumer, File... files) throws IOException {
        lines(lineConsumer, "UTF-8", files);
    }

    public static void lines(Consumer<String> lineConsumer, String encoding, File... files) throws IOException {
        for (File lemmaDictFile : files) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lemmaDictFile), encoding))) {
                String line;
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    if (!line.isEmpty() && !line.startsWith("#")) {
                        // lines line
                        lineConsumer.accept(line);
                    }
                }
            }
        }
    }

    public static void tsvRows(Consumer<String[]> rowConsumer, File... files) throws IOException {
        csvRows(" *[\t] *", true, rowConsumer, "UTF-8", files);
    }

    public static void csvRows(Consumer<String[]> rowConsumer, File... files) throws IOException {
        csvRows(" *[,\t] *", true, rowConsumer, "UTF-8", files);
    }

    public static void csvRows(String separator, boolean unquote, Consumer<String[]> rowConsumer, String encoding, File... files) throws IOException {
        lines(line -> {
            String[] vals = line.split(separator);
            if(unquote) {
                for (int i = 0; i < vals.length; i++) {
                    vals[i] = unquote(vals[i]);
                }
                rowConsumer.accept(vals);
            }
        }, encoding, files);
    }

    /**
     * This String util method removes single or double quotes
     * from a string if its quoted.
     * for input string = "mystr1" output will be = mystr1
     * for input string = 'mystr2' output will be = mystr2
     *
     * @param s to be unquoted.
     * @return value unquoted, null if input is null.
     *
     */
    public static String unquote(String s) {
        if (s != null
                && ((s.startsWith("\"") && s.endsWith("\""))
                || (s.startsWith("'") && s.endsWith("'"))))
        {

            s = s.substring(1, s.length() - 1);
        }
        return s;
    }
}
