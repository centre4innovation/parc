package org.humanityx.nlp.classification;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.util.CoreMap;
import opennlp.tools.namefind.TokenNameFinderModel;
import org.humanityx.nlp.classification.pojo.NLPInfo;
import org.humanityx.nlp.classification.pojo.NamedEntity;
import org.humanityx.nlp.classification.pojo.Sentence;
import org.humanityx.nlp.classification.pojo.Token;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 */
class NLPEnglish implements INLP {
    //For the language support see: http://stanfordnlp.github.io/CoreNLP/human-languages.html
    // and here for the version support 3.6.0 http://stanfordnlp.github.io/CoreNLP/history.html
    //private Properties props;
    //private StanfordCoreNLP pipeline;
    private AnnotationPipeline pipeline;
    private StopWords stopWords;
    private final List<String> charsToRemove = Arrays.asList(":", ",", "'", ",", "-", ".", "?", "!", ";", "-", "/", "\\",
            "­­­­­­­­­­­­­­­­­­­­", "\n", "\r", "\t", "\"", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "%", "(", ")", "@", "&", "*", "‘", "“", "’", "–", "­", "|", ">", "<", "{", "}", "[", "]", "+", "¤", "$",
            "^", "#", "''", "``", "`",
            "-RSB-", "-LSB-", "-RCB-", "-LCB-", "-RRB-", "-LRB-");
    private final List<String> numbers = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    private TokenNameFinderModel modelPerson;
    private TokenNameFinderModel modelOrganization;
    private TokenNameFinderModel modelLocation;
    private TokenNameFinderModel modelDate;
    private TokenNameFinderModel modelTime;
    private TokenNameFinderModel modelMoney;

    public NLPEnglish() throws IOException {
        setPipeline();

        //load models

        //The NameFinderME class is not thread safe, it must only be called from one thread.
        //To use multiple threads multiple NameFinderME instances sharing the same model instance can be created.
        //https://opennlp.apache.org/documentation/1.6.0/manual/opennlp.html
        InputStream modelInPerson = new FileInputStream("data/nlp/en/en-ner-person.bin");
        modelPerson = new TokenNameFinderModel(modelInPerson);

        InputStream modelInOrganization = new FileInputStream("data/nlp/en/en-ner-organization.bin");
        modelOrganization = new TokenNameFinderModel(modelInOrganization);

        InputStream modelInLocation = new FileInputStream("data/nlp/en/en-ner-location.bin");
        modelLocation = new TokenNameFinderModel(modelInLocation);

        InputStream modelInDate = new FileInputStream("data/nlp/en/en-ner-date.bin");
        modelDate = new TokenNameFinderModel(modelInDate);

        InputStream modelInTime = new FileInputStream("data/nlp/en/en-ner-time.bin");
        modelTime = new TokenNameFinderModel(modelInTime);

        InputStream modelInMoney = new FileInputStream("data/nlp/en/en-ner-money.bin");
        modelMoney = new TokenNameFinderModel(modelInMoney);

        this.stopWords = new StopWords(new File("data/nlp/en/stopwords_en.txt"));

    }

    private void setPipeline() {
        pipeline = null;

        //        this.props = new Properties();
        //        props.setProperty("maxLength", "70"); //setting the max length of a sentence
        //        props.setProperty("parse.maxlen", "100");
        //        props.setProperty("ner.model", "edu/stanford/nlp/models/ner/english.all.3class.distsim.crf.ser.gz");
        //        props.setProperty("ner.maxlen", "100");
        //        props.setProperty("ner.useSUTime", "false");
        //        props.setProperty("ner.applyNumericClassifiers", "false");
        AnnotationPipeline pipeline = new AnnotationPipeline();
        pipeline.addAnnotator(new TokenizerAnnotator(false, "en"));
        pipeline.addAnnotator(new WordsToSentencesAnnotator(false));
        pipeline.addAnnotator(new POSTaggerAnnotator(false)); //I suspect that this library has a memory leak problem
        pipeline.addAnnotator(new MorphaAnnotator(false)); //lemma
        //pipeline.addAnnotator(new NERCombinerAnnotator(false)); //DO NOT LOAD THE NER, IT HAS A MEMORY LEAK PROBLEM
        this.pipeline = pipeline;

    }


    @Override
    public NLPInfo getInfo(String text) throws InterruptedException {
        List<Sentence> sentenceList = new ArrayList<>();
        List<String> lemmaList = new ArrayList<>();
        List<NamedEntity> entityList = new ArrayList<>();
        NameFinder personFinder = new NameFinder("PERSON", modelPerson);
        NameFinder organizationFinder = new NameFinder("ORGANIZATION", modelOrganization);
        NameFinder locationFinder = new NameFinder("LOCATION", modelLocation);
        NameFinder timeFinder = new NameFinder("TIME", modelTime);
        NameFinder dateFinder = new NameFinder("DATE", modelDate);
        NameFinder moneyFinder = new NameFinder("MONEY", modelMoney);


        Annotation document = new Annotation(text);
        if (pipeline == null) {
            Thread.sleep(5000);
        }
        pipeline.annotate(document);

        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

        for (CoreMap sentence : sentences) {
            List<Token> tokens = new ArrayList<>();
            List<NamedEntity> entitiesSentence = new ArrayList<>();

            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                //Get information
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                //String ner = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);

                //Process dots, quotes, commas, etc.
                boolean skipWord = false;
                if (charsToRemove.stream().anyMatch(str -> str.trim().equals(word)) ||
                        numbers.stream().anyMatch(str -> word.contains(str)) ||
                        stopWords.isStopWord(word) ||
                        stopWords.isStopWord(lemma)) {
                    skipWord = true;
                }

                if (!skipWord) {
                    //Add lemma to the list.
                    lemmaList.add(lemma);
                    //Process entity
                    tokens.add(new Token(-1, word, pos, lemma));
                }
            }

            //entities
            entitiesSentence.addAll(personFinder.find(sentence));
            entitiesSentence.addAll(locationFinder.find(sentence));
            entitiesSentence.addAll(organizationFinder.find(sentence));
            entitiesSentence.addAll(timeFinder.find(sentence));
            entitiesSentence.addAll(dateFinder.find(sentence));
            entitiesSentence.addAll(moneyFinder.find(sentence));

            sentenceList.add(new Sentence(sentence.toString(), tokens, entitiesSentence));
            entityList.addAll(entitiesSentence);
        }

        personFinder.clearData();//Needs to be done after every document
        locationFinder.clearData();
        organizationFinder.clearData();


        return new NLPInfo(lemmaList, entityList, sentenceList);
    }

    public StopWords getStopWords() {
        return stopWords;
    }

}
