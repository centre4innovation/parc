package org.humanityx.nlp.classification;

import org.humanityx.nlp.classification.db.DAO;
import org.humanityx.nlp.classification.pojo.Classification;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by Wouter Eekhout on 19/01/2017.
 * This class stores in memory the already retrieved information from the database.
 */
class TfIdfValue {
    private DAO dao;
    private HashMap<Integer, Classification> classifications;
    private long countClassifications;
    private HashMap<Integer, HashMap<String, HashMap<String, Double>>> tfValues;
    private HashMap<String, HashMap<String, Double>> idfValues;
    //private DAO dao;

    TfIdfValue(DAO dao, List<Classification> classifications) throws Exception {
        this.dao = dao;
        this.classifications = new HashMap<>();
        this.tfValues = new HashMap<>();
        this.idfValues = new HashMap<>();
        for(Classification classification : classifications) {
            this.classifications.put(classification.getId(), classification);
            this.tfValues.put(classification.getId(), new HashMap<>());
        }
        this.countClassifications = this.classifications.size();
        if(this.countClassifications == 0) {
            throw new Exception("It needs to have classifications");
        }
    }

    public double getTfIdfValue(int classificationId, String token, String pos) throws SQLException {
        double tfValue = this.getTfValue(classificationId, token, pos);
        double idfValue = this.getIdfValue(token, pos);
        double tfIdValue = tfValue * idfValue;
        return tfIdValue;
    }

    double getIdfValue(String token, String pos) throws SQLException {
        if(!this.idfValues.containsKey(token)) {
            addIdfValue(token, pos);
        }
        if(!idfValues.get(token).containsKey(pos)) {
            addIdfValue(token, pos);
        }
        return idfValues.get(token).get(pos);
    }

    private void addIdfValue(String token, String pos) throws SQLException {
        if(!idfValues.containsKey(token)) {
            idfValues.put(token, new HashMap<>());
        }

        if(idfValues.get(token).containsKey(pos)) {
            return; //already has the value
        }

        long countInClassifications = this.dao.getClassificationTokenDAO().countTokenInClassification(token, pos);
        double idfValue = 0;
        if(countInClassifications > 0) {
            idfValue = Math.log(this.countClassifications / countInClassifications);
        }
        idfValues.get(token).put(pos, idfValue);
    }

    double getTfValue(int classification, String token, String pos) throws SQLException {
        if(!tfValues.get(classification).containsKey(token)) { //we assume that classification already exists
            addTfValue(classification, token, pos);
        }
        if(!tfValues.get(classification).get(token).containsKey(pos)) {
            addTfValue(classification, token, pos);
        }

        return tfValues.get(classification).get(token).get(pos);
    }

    private void addTfValue(int classificationId, String token, String pos) throws SQLException {
        if(!tfValues.get(classificationId).containsKey(token)) { //we assume that classification already exists
            tfValues.get(classificationId).put(token, new HashMap<>());
        }

        if(tfValues.get(classificationId).get(token).containsKey(pos)) {
            return; //value already exists
        }

        double tfValue = 0;
        double val = 0;

        try {
            val = this.dao.getClassificationTokenDAO().getTokenFrequencyInClassification(classificationId, token, pos);
        } catch (Exception ignored) { } //value does not exist in the database

        Classification classification = this.classifications.get(classificationId);
        if(val != 0 && classification.getTotalTokenFreq() > 0) {
            tfValue = val / classification.getTotalTokenFreq();
        }

        tfValues.get(classificationId).get(token).put(pos, tfValue);
    }


}
