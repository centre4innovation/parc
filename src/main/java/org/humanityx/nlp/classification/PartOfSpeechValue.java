package org.humanityx.nlp.classification;

import java.util.HashMap;

/**
 * Created by Wouter Eekhout on 22/11/2017.
 *
 */
class PartOfSpeechValue {
    private HashMap<String, HashMap<String, Double>> values;
    private HashMap<String, Double> defaultValues;
    //POS scores
    //POS:      Noun  Verb    Adjunctive  SPEC (names)      Other
    //VALUE:    1     1       1           1                 0

    PartOfSpeechValue() {
        values = new HashMap<>();

        //english
        loadEnglishPosValues();
    }

    private void loadEnglishPosValues() {
        HashMap<String, Double> languageValues = new HashMap<>();
        //nouns
        languageValues.put("NN", 1.0);
        languageValues.put("NNS", 1.0);
        languageValues.put("NNP", 1.0);
        languageValues.put("NNPS", 1.0);
        //verbs
        languageValues.put("VB", 1.0);
        languageValues.put("VBD", 1.0);
        languageValues.put("VBG", 1.0);
        languageValues.put("VBN", 1.0);
        languageValues.put("VBP", 1.0);
        languageValues.put("VBZ", 1.0);
        //adjunctives
        languageValues.put("JJ", 1.0);
        languageValues.put("JJR", 1.0);
        languageValues.put("JJS", 1.0);

        values.put("en", languageValues);
        defaultValues = languageValues;
    }

    public double getValue(String language, String pos) {
        if(!values.containsKey(language)) {
            return 0;
        }
        HashMap<String, Double> languageValues = values.get(language);
        if(!languageValues.containsKey(pos)) {
            return 0;
        }
        return languageValues.get(pos);
    }

    public double getValue(String pos) {
        if(!defaultValues.containsKey(pos)){
            return 0;
        }
        return defaultValues.get(pos);
    }
}
