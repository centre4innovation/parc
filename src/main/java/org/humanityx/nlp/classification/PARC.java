package org.humanityx.nlp.classification;

import org.humanityx.nlp.classification.db.ClassificationDAO;
import org.humanityx.nlp.classification.db.ClassificationTokenDAO;
import org.humanityx.nlp.classification.db.Connect;
import org.humanityx.nlp.classification.db.DAO;
import org.humanityx.nlp.classification.pojo.NLPInfo;
import org.humanityx.nlp.classification.pojo.Sentence;
import org.humanityx.nlp.classification.pojo.Token;
import org.humanityx.nlp.classification.pojo.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.isNaN;

/**
 * Created by Wouter Eekhout on 17/01/2017.
 *
 */
public class PARC {
    private DAO dao;
    private List<Classification> classifications;
    private TfIdfValue tfIdfValue;
    private PartOfSpeechValue posValue;

    /**
     * The path to the sqlite database location
     * @param pathToDb path to the sqlite database file, if none exists the file will be created
     * @throws Exception
     */
    public PARC(String pathToDb) throws Exception {
        initDAO(pathToDb);
    }

    public PARC() throws SQLException {
        initDAO("data/db/database.sqlite");
    }

    /**
     * Sets the classifications in memory needed for predicting and training
     * @throws Exception
     */
    public void init() throws Exception {
        this.classifications = this.dao.getClassificationDAO().get();

        this.tfIdfValue = new TfIdfValue(dao, this.classifications);
        this.posValue = new PartOfSpeechValue();
    }

    private void initDAO(String path) throws SQLException {
        Connect connect = new Connect(path);
        ClassificationDAO classificationDAO = new ClassificationDAO(connect);

        try {
            classificationDAO.ensureTable();
        } catch (SQLException ignored) {}

        ClassificationTokenDAO classificationTokenDAO = new ClassificationTokenDAO(connect);

        try {
            classificationTokenDAO.ensureTable();
        } catch (SQLException ignored) {}

        this.dao = new DAO(classificationTokenDAO, classificationDAO);
    }

    /**
     * This method learns per classification from the input text
     * @param nlp the NLP class
     * @param text the text associated with the classification
     * @param classificationId the id of the classification
     * @throws InterruptedException
     * @throws SQLException
     */
    public void train(INLP nlp, String text, int classificationId) throws InterruptedException, SQLException {
        NLPInfo nlpInfo = nlp.getInfo(text);
        StopWords stopWords = nlp.getStopWords();
        ClassificationTokenDAO classificationTokenDAO = this.dao.getClassificationTokenDAO();
        int totalAddedTokens = 0;

        for (Sentence sentence : nlpInfo.getSentences()) {
            //train from sentence
            for(Token token : sentence.getTokens()) {
                if(stopWords.isStopWord(token.getText()) || stopWords.isStopWord(token.getLemma())) {
                    continue; //ignore stopwords for learning
                }

                if(!classificationTokenDAO.exists(classificationId, token.getLemma(), token.getPos())) {
                    classificationTokenDAO.insert(classificationId, token.getLemma(), token.getPos());
                }

                classificationTokenDAO.addFrequency(classificationId,
                        token.getLemma(), token.getPos(), 1); //add the freq
                totalAddedTokens++; //add to the total freq
            }
        }

        this.dao.getClassificationDAO().addFrequency(classificationId, totalAddedTokens);
    }

    /**
     * Classifies the text into
     * @param nlp the nlp
     * @param text the text that needs to be classified
     * @param minimumScore the minimum score that a classification needs to have in order to be returned. Value is from 0 - 1. Recommended: 0.3
     * @param minimumAverageWordWeight the minimum average of all the words that the classification needs to have in order to be returned
     *                                 recommended is: 0.01
     * @param minimumHighScoringWordWeight the value that the highest scoring words needs to have in order to have the classification returned
     *                                     recommended is: 0.1
     * @return A list of predictions
     * @throws InterruptedException
     * @throws SQLException throws when the database cannot be accessed or a query failed
     */
    public ClassificationScores predict(INLP nlp, String text, double minimumScore, double minimumAverageWordWeight,
                                        double minimumHighScoringWordWeight) throws InterruptedException, SQLException {
        NLPInfo nlpInfo = nlp.getInfo(text);
        List<Long> predictions = new ArrayList<>();


        //if (this.classifications.size() < 10 || !this.settings.isActive()) {
        if (this.classifications.size() < 10) {
            List<ClassificationScore> classificationScores = new ArrayList<>();
            return new ClassificationScores(predictions, classificationScores); //PARC does not have enough information to process the information
        }
        StopWords stopWords = nlp.getStopWords();

        List<ClassificationScore> classificationScores = calculateScores(nlpInfo, stopWords);

        for (ClassificationScore classificationScore : classificationScores) {
            if (classificationScore.getScore() >= minimumScore) {
                //alright this seems promising
                if (classificationScore.getAverageTokenWeight() >= minimumAverageWordWeight
                        && classificationScore.getHighestTokenWeight() >= minimumHighScoringWordWeight) {
                    predictions.add(classificationScore.getClassification());//we got one!
                }
            }
        }

        if(predictions.size() > 4) {
            //More than 4 classifications prediction make no sense
            predictions = new ArrayList<>();
        }

        return new ClassificationScores(predictions, classificationScores);
    }

    private List<ClassificationScore> calculateScores(NLPInfo nlpInfo, StopWords stopWords) throws SQLException {
        List<ClassificationScore> classificationScores = new ArrayList<>();
        for(Classification classification : this.classifications) {
            ClassificationScore classificationScore = new ClassificationScore(classification.getId());

            for (Sentence sentence : nlpInfo.getSentences()) {
                //get values
                for(Token token : sentence.getTokens()) {
                    if(stopWords.isStopWord(token.getText()) || stopWords.isStopWord(token.getLemma())) {
                        continue; //ignore stopwords
                    }

                    double tfIdfWeight = this.tfIdfValue.getTfIdfValue(classification.getId(), token.getLemma(), token.getPos());
                    double posWeight = this.posValue.getValue(token.getPos());
                    double tokenWeight = tfIdfWeight * posWeight;

                    classificationScore.addScore(tokenWeight);
                    classificationScore.addWeight(new TokenWeight(tfIdfWeight, posWeight, tokenWeight, token.getText(),
                            token.getLemma(), token.getPos()));
                }
            }

            double predictionScore = classificationScore.getScore() / classificationScore.getTokenWeights().size();
            if(isNaN(predictionScore)) {
                predictionScore = 0;
            }
            classificationScore.setScore(predictionScore);

            classificationScores.add(classificationScore);

            this.l2Normalize(classificationScore); //normalize token weights
        }
        this.l2Normalize(classificationScores); //normalize score
        return classificationScores;
    }

    /**
     * PARC saves a lot in memory to speed up. Use this method to unload the data
     * @throws Exception
     */
    public void unload() throws Exception {
        this.tfIdfValue = new TfIdfValue(this.dao, this.classifications);
    }

    private void l2Normalize(List<ClassificationScore> classificationScores) {
        Double sumSquared = 0.0;
        for(ClassificationScore classificationScore : classificationScores){
            sumSquared += classificationScore.getScore() * classificationScore.getScore();
        }

        Double sqrtSumSquared = Math.sqrt(sumSquared);

        for(ClassificationScore classificationScore : classificationScores){
            Double normalizedScore = classificationScore.getScore() / sqrtSumSquared;
            if(isNaN(normalizedScore)) {
                normalizedScore = 0.0;
            }
            classificationScore.setScore(normalizedScore);
        }
    }

    private void l2Normalize(ClassificationScore classificationScore) {
        double sumSquared = 0.0;
        for(TokenWeight tokenWeight : classificationScore.getTokenWeights()) {
            sumSquared += tokenWeight.getTotalWeight() * tokenWeight.getTotalWeight();
        }

        double sqrtSumSquared = Math.sqrt(sumSquared);

        for(TokenWeight tokenWeight : classificationScore.getTokenWeights()) {
            double normalizedScore = tokenWeight.getTotalWeight() / sqrtSumSquared;
            if(isNaN(normalizedScore)) {
                normalizedScore = 0.0;
            }
            tokenWeight.setTotalWeight(normalizedScore);
        }
    }

    public DAO getDao() {
        return dao;
    }
}
