package org.humanityx.nlp.classification;

import org.humanityx.nlp.classification.pojo.NLPInfo;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 */
interface INLP {
    NLPInfo getInfo(String text) throws InterruptedException;

    StopWords getStopWords();
}
