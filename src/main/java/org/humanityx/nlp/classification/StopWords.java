package org.humanityx.nlp.classification;

import org.apache.commons.lang3.StringUtils;
import org.humanityx.nlp.classification.util.LineParser;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Arvid Halma extended by Wouter Eekhout on 14/11/2016.
 *
 */
class StopWords {
    private Set<String> stopWords;
    public final static StopWords EMPTY_STOPWORDS = new StopWords();

    private StopWords(){
        this.stopWords = Collections.<String>emptySet();
    }

    public StopWords(File... stopWordFiles) throws IOException {
        this.stopWords = new HashSet<>();
        LineParser.lines(line -> {
            stopWords.add(line);
        }, stopWordFiles);
    }

    public Set<String> getStopWords() {
        return stopWords;
    }


    public boolean isStopWord(String word){
        return stopWords.contains(word.toLowerCase());
    }

    public List<Boolean> isStopWord(Collection<String> words){
        return words.stream().map(this::isStopWord).collect(Collectors.toList());
    }

    public Predicate<String> stopWordPredicate(){
        return this::isStopWord;
    }

    public Collection<String> filter(Collection<String> words){
        words.removeIf(this::isStopWord);
        return words;
    }

    public List<String> filter(List<String> words){
        words.removeIf(this::isStopWord);
        return words;
    }


    public List<String> filterWords(List<String> words) {
        words.removeIf(this::isStopWord);
        return words;
    }

    public String filterText(String text) {
        String patternString = "(?i)\\b(" + StringUtils.join(stopWords, "|") + ")\\b";  //case insenstive
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);
        return matcher.replaceAll("");
    }

    public static <T> Predicate<T> not(Predicate<T> t) {
        return t.negate();
    }

    @Override
    public String toString() {
        return "StopWords" + stopWords;
    }
}