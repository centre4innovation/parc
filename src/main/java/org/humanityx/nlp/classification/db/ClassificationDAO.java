package org.humanityx.nlp.classification.db;

import org.humanityx.nlp.classification.pojo.Classification;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wouter Eekhout on 28/11/2016.
 */
public class ClassificationDAO {
    private Statement statement;

    public ClassificationDAO(Connect connect) {
        this.statement = connect.getStatement();
    }

    public void drop() throws SQLException {
        statement.executeUpdate(//language=SQL
                "DROP TABLE IF EXISTS classification;");

    }

    public void ensureTable() throws SQLException {
        statement.executeUpdate("CREATE TABLE classification\n" +
                "(\n" +
                "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "    name VARCHAR(500),\n" +
                "    total_token_freq INTEGER DEFAULT 0\n" +
                ");");
    }

    public int insert(String name) throws SQLException {
        statement.executeUpdate(String.format(//language=SQL
                "INSERT INTO classification(name, total_token_freq) " +
                "VALUES ('%s', 0);", name));
        ResultSet rs = statement.getGeneratedKeys();

        if (rs.next()) {
            int id = rs.getInt(1);
            rs.close();
            return id;
        }

        throw new SQLException("Insert failed");
    }

    public void update(int id, String name, int totalTokenFreq) throws SQLException {
        statement.executeUpdate(String.format(//language=SQL
                "UPDATE classification\n" +
                "   SET name='%s', total_token_freq=%d \n" +
                " WHERE id=%d;", name, totalTokenFreq, id));
    }

    public void delete(int id) throws SQLException {
        statement.executeUpdate(String.format(//language=SQL
                "DELETE FROM classification WHERE id = %d;", id));
    }

    public List<Classification> get() throws SQLException {
        List<Classification> result = new ArrayList<>();

        ResultSet rs = statement.executeQuery("SELECT * FROM classification\n" +
                "ORDER BY name ASC;");

        while (rs.next()) {
            result.add(getClassification(rs));
        }

        rs.close();

        return result;
    }

    public Classification get(int id) throws SQLException {
        ResultSet rs = statement.executeQuery(
                String.format(//language=SQL
                        "SELECT * FROM classification WHERE id=%d ORDER BY name ASC;", id));

        if (rs.next()) {
            Classification classification = getClassification(rs);
            rs.close();
            return classification;
        }

        throw new SQLException("Get failed");
    }

    public void addFrequency(int id, int addFrequency) throws SQLException {
        statement.executeUpdate(//language=SQL
                String.format("UPDATE classification SET total_token_freq = (total_token_freq+%d)" +
                        "WHERE id = %d;", addFrequency, id));
    }

    private Classification getClassification(ResultSet rs) throws SQLException {
        return new Classification(rs.getInt("id"),
                rs.getString("name"), rs.getInt("total_token_freq"));
    }
}
