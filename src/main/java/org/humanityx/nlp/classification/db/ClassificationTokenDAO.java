package org.humanityx.nlp.classification.db;

import org.humanityx.nlp.classification.pojo.ClassificationToken;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Wouter Eekhout on 17/01/2017.
 */
public class ClassificationTokenDAO {
    private Statement statement;

    public ClassificationTokenDAO(Connect connect) {
        this.statement = connect.getStatement();
    }

    public void drop() throws SQLException {
        statement.executeUpdate(//language=SQL
                "DROP TABLE IF EXISTS classification_token;");

    }

    public void ensureTable() throws SQLException {
        statement.executeUpdate("CREATE TABLE classification_token\n" +
                "(\n" +
                "    id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "    classification INTEGER,\n" +
                "    token VARCHAR(50),\n" +
                "    pos VARCHAR(20),\n" +
                "    frequency INTEGER DEFAULT 0\n" +
                ");");

        statement.executeUpdate("CREATE INDEX classification_token_classification_index ON classification_token (classification);");
        statement.executeUpdate("CREATE INDEX classification_token_token_index ON classification_token (token);");
        statement.executeUpdate("CREATE INDEX classification_token_pos_index ON classification_token (pos);");
        statement.executeUpdate("CREATE INDEX classification_token_token_pos_index ON classification_token (token, pos);");
        statement.executeUpdate("CREATE INDEX classification_token_classification_token_pos_index ON classification_token (classification, token, pos);");
    }

    public int insert(int classification, String token, String pos) throws SQLException {
        statement.executeUpdate(String.format(//language=SQL
                "INSERT INTO classification_token(classification, token, pos) \n" +
                        "VALUES (%d, '%s', '%s')", classification, token, pos));
        ResultSet rs = statement.getGeneratedKeys();

        if (rs.next()) {
            int id = rs.getInt(1);
            rs.close();
            return id;
        }

        throw new SQLException("Insert failed");
    }

    public boolean exists(int classification, String token, String pos) throws SQLException {
        ResultSet rs = statement.executeQuery(String.format(//language=SQL
                "SELECT EXISTS(SELECT * FROM classification_token " +
                "WHERE classification = %d AND token = '%s' AND pos = '%s');", classification, token, pos));

        if(rs.next()) {
            boolean exists = rs.getBoolean(1);
            rs.close();
            return exists;
        }

        throw new SQLException("No result");
    }

    public ClassificationToken get(int classification, String token, String pos) throws SQLException {
        ResultSet rs = statement.executeQuery(
                String.format(//language=SQL
                        "SELECT * FROM classification_token \n" +
                                "WHERE classification = %d AND token = '%s' AND pos = '%s'\n" +
                                "LIMIT 1;", classification, token, pos));

        if (rs.next()) {
            ClassificationToken result = getClassificationToken(rs);
            rs.close();
            return result;
        }

        throw new SQLException("Get failed");
    }

    public void addFrequency(int classification, String token, String pos, int addFrequency) throws SQLException {
        statement.executeUpdate(//language=SQL
                String.format("UPDATE classification_token SET frequency = (frequency+%d)" +
                        "WHERE classification = %d AND token = '%s' AND pos = '%s';", addFrequency, classification,
                        token, pos));
    }

    public void deleteByClassification(int classification) throws SQLException {
        statement.executeUpdate(String.format(//language=SQL
                "DELETE FROM classification_token WHERE classification = %d;", classification ));
    }

    public int countTokenInClassification(String token, String pos) throws SQLException {
        ResultSet rs = statement.executeQuery(//language=SQL
                String.format("SELECT COUNT(DISTINCT classification)\n" +
                        "FROM classification_token\n" +
                        "WHERE token = '%s' AND pos = '%s';", token, pos));

        if(rs.next()) {
            int exists = rs.getInt(1);
            rs.close();
            return exists;
        }

        throw new SQLException("No result");
    }

    public int getTokenFrequencyInClassification(int classification, String token, String pos) throws SQLException {
        ResultSet rs = statement.executeQuery(//language=SQL
                String.format("SELECT frequency FROM classification_token\n" +
                        "WHERE classification = %d AND token = '%s' AND pos = '%s';", classification, token, pos));

        if(rs.next()) {
            int exists = rs.getInt(1);
            rs.close();
            return exists;
        }

        throw new SQLException("No result");
    }

    private ClassificationToken getClassificationToken(ResultSet rs) throws SQLException {
        return new ClassificationToken(rs.getInt("id"),
                rs.getInt("classification"), rs.getString("token"),
                rs.getString("pos"), rs.getInt("frequency"));
    }
}

