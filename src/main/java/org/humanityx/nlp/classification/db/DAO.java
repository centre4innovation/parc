package org.humanityx.nlp.classification.db;

import lombok.Data;

@Data
public class DAO {
    private ClassificationTokenDAO classificationTokenDAO;
    private ClassificationDAO classificationDAO;

    public DAO(ClassificationTokenDAO classificationTokenDAO, ClassificationDAO classificationDAO) {
        this.classificationTokenDAO = classificationTokenDAO;
        this.classificationDAO = classificationDAO;
    }
}
