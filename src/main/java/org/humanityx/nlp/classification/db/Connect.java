package org.humanityx.nlp.classification.db;

import java.sql.*;

/**
 * Created by Wouter Eekhout on 20/11/2017.
 * https://github.com/xerial/sqlite-jdbc
 */
public class Connect {
    private Connection connection;
    private Statement statement;

    /**
     *  Initialises the database connection
     * @param path path to database location e.g. data/db/database.sqlite
     */
    public Connect(String path) throws SQLException {
        createConnection(path);
    }

    public Connect() throws SQLException {
        createConnection("data/db/database.sqlite");
    }

    private void createConnection(String path) throws SQLException {
        // create a database connection
        connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s", path));
        statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
    }

    public Connection getConnection() {
        return this.connection;
    }

    Statement getStatement() {
        return this.statement;
    }
}
