package org.humanityx.nlp.classification;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.CoreMap;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;
import org.humanityx.nlp.classification.pojo.NamedEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wouter Eekhout on 30/11/2016.
 */
class NameFinder {
    private String type;
    private NameFinderME nameFinderME;

    public NameFinder(String type, TokenNameFinderModel model) {
        this.type = type;
        nameFinderME = new NameFinderME(model);
    }

    public List<NamedEntity> find(String[] tokens) {
        List<NamedEntity> result = new ArrayList<>();

        Span nameSpans[] = nameFinderME.find(tokens);

        String[] nameSpansStr = Span.spansToStrings(nameSpans, tokens);
        for( int i = 0; i< nameSpansStr.length; i++) {
            String text = nameSpansStr[i];
            if(text.length() < 3) {
                continue;
            }
            result.add(new NamedEntity(-1, text, type));
        }

        return result;
    }

    public List<NamedEntity> find(List<CoreMap> sentences) {
        List<NamedEntity> result = new ArrayList<>();

        for (CoreMap sentence : sentences) {
            result.addAll(find(sentence));
        }
        nameFinderME.clearAdaptiveData(); //After every document

        return result;
    }

    public List<NamedEntity> find(CoreMap sentence) {
        List<NamedEntity> result = new ArrayList<>();

        List<String> tokens = new ArrayList<>();
        for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
            tokens.add(token.get(CoreAnnotations.TextAnnotation.class));
        }

        String[] tokensArray = tokens.toArray(new String[0]);
        result.addAll(find(tokensArray));

        return result;
    }

    public void clearData() {
        nameFinderME.clearAdaptiveData();
    }
}
