package org.humanityx.nlp.classification.pojo;

import lombok.Data;

import java.util.*;

/**
 * Created by Wouter Eekhout on 20/01/2017.
 */
@Data
public class ClassificationScore {
    private long classification;
    private double score;
    private List<TokenWeight> tokenWeights;
    private double totalTokenWeights;
    private double highestTokenWeight;

    public ClassificationScore(long classification) {
        this.classification = classification;
        this.score = 0;
        this.tokenWeights = new ArrayList<>();
    }

    public void addScore(double score) {
        this.score += score;
    }
    public void setScore(double score) {
        this.score = score;
    }

    public void addWeight(TokenWeight tokenWeight) {
        this.tokenWeights.add(tokenWeight);
        this.totalTokenWeights += tokenWeight.getTotalWeight();
    }

    public double getHighestTokenWeight() {
        Optional result = this.tokenWeights.stream().max(Comparator.comparingDouble(TokenWeight::getTotalWeight));
        if(result.isPresent()) {
            return ((TokenWeight)result.get()).getTotalWeight();
        }
        return 0.0;
    }

    public double getAverageTokenWeight() {
        return this.tokenWeights.stream().mapToDouble(p -> p.getTotalWeight()).sum() / this.tokenWeights.size();
    }
}
