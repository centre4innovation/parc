package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 20/01/2017.
 */
@Data
public class TokenWeight {
    private double tfIdfValue;
    private double posWeight;
    private double totalWeight;
    private String token;
    private String lemma;
    private String pos;

    public TokenWeight(double tfIdfValue, double posWeight, double totalWeight, String token, String lemma, String pos) {
        this.tfIdfValue = tfIdfValue;
        this.posWeight = posWeight;
        this.totalWeight = totalWeight;
        this.token = token;
        this.lemma = lemma;
        this.pos = pos;
    }
}
