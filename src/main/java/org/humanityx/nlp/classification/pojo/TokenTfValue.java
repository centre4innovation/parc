package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 19/01/2017.
 */
@Data
public class TokenTfValue {
    private String token;
    private String pos;
    private Double tfValue;

    public TokenTfValue(String token, String pos, Double tfValue) {
        this.token = token;
        this.pos = pos;
        this.tfValue = tfValue;
    }

}
