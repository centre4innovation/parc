package org.humanityx.nlp.classification.pojo;

import lombok.Data;

import java.util.List;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 */
@Data
public class NLPInfo {
    private List<String> lemmas;
    private List<NamedEntity> entities;
    private List<Sentence> sentences;

    public NLPInfo(List<String> lemmas, List<NamedEntity> entities, List<Sentence> sentences) {
        this.lemmas = lemmas;
        this.entities = entities;
        this.sentences = sentences;
    }

}
