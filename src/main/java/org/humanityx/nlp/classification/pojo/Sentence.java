package org.humanityx.nlp.classification.pojo;

import lombok.Data;

import java.util.List;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 */
@Data
public class Sentence {
    private List<Token> tokens;
    private List<NamedEntity> entities;
    private String text;

    public Sentence(String text, List<Token> tokens, List<NamedEntity> entities) {
        this.text = text;
        this.tokens = tokens;
        this.entities = entities;
    }
}
