package org.humanityx.nlp.classification.pojo;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlWriter;
import lombok.Data;

import java.io.StringWriter;
import java.util.List;

/**
 * Created by Wouter Eekhout on 20/01/2017.
 */
@Data
public class ClassificationScores {
    private List<Long> predictions;
    private List<ClassificationScore> scores;

    public ClassificationScores(List<Long> predictions, List<ClassificationScore> scores) {
        this.predictions = predictions;
        this.scores = scores;
    }

    public String getScoresString() throws YamlException {
        StringBuilder stringBuilder = new StringBuilder();
        for(ClassificationScore score : this.scores) {
            StringWriter stringWriter = new StringWriter();
            YamlWriter writer = new YamlWriter(stringWriter);

            writer.write(score);
            writer.close();

            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(stringWriter.toString());

        }
        return stringBuilder.toString();
    }

    public String getPredictionsString() throws YamlException {
        StringWriter stringWriter = new StringWriter();
        YamlWriter writer = new YamlWriter(stringWriter);

        writer.write(this.predictions);
        writer.close();

        return stringWriter.toString();
    }
}
