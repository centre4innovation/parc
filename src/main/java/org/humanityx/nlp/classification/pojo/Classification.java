package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 28/11/2016.
 */
@Data
public class Classification {
    private int id;
    private String name;
    private long totalTokenFreq;

    public Classification(int id, String name, long totalTokenFreq) {
        this.id = id;
        this.name = name;
        this.totalTokenFreq = totalTokenFreq;
    }

}
