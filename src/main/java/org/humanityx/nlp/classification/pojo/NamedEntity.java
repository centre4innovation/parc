package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 */
@Data
public class NamedEntity {
    private long id;
    private long article;
    private String text;
    private String entity;

    public NamedEntity(long id, String text, String entity) {
        this.id = id;
        this.text = text;
        this.entity = entity;
    }

    public NamedEntity(long id, long article, String text, String entity) {
        this.id = id;
        this.article = article;
        this.text = text;
        this.entity = entity;
    }
}
