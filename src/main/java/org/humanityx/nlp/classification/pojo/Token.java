package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 10/11/2016.
 *
 */
@Data
public class Token {
    private long id;
    private long article;
    private String text;
    private String pos;
    private String lemma;

    public Token(long id, String text, String pos, String lemma) {
        this.id = id;
        this.text = text;
        this.pos = pos;
        this.lemma = lemma;
    }

    public Token(long id, long article, String text, String pos, String lemma) {
        this.id = id;
        this.article = article;
        this.text = text;
        this.pos = pos;
        this.lemma = lemma;
    }
}
