package org.humanityx.nlp.classification.pojo;

import lombok.Data;

/**
 * Created by Wouter Eekhout on 17/01/2017.
 */
@Data
public class ClassificationToken {
    private int id;
    private int classification;
    private String token;
    private String pos;
    private int frequency;

    public ClassificationToken(int id, int classification, String token, String pos, int frequency) {
        this.id = id;
        this.classification = classification;
        this.token = token;
        this.pos = pos;
        this.frequency = frequency;
    }

}
