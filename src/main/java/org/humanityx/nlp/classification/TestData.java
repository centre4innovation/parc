package org.humanityx.nlp.classification;

/**
 * Created by Wouter Eekhout on 22/11/2017.
 *
 *  This class contains training data according of the 17 Sustainable Development Goals
 */
public class TestData {
    public final static String sustainableDevelopmentGoal1 = "Goal 1: End poverty in all its forms everywhere\n" +
            "Extreme poverty rates have been cut by more than half since 1990. \n" +
            "While this is a remarkable achievement, one in five people in developing regions still live on less than $1.25 a day, and there are millions more who make little more than this daily amount, plus many people risk slipping back into poverty.\n" +
            "Poverty is more than the lack of income and resources to ensure a sustainable livelihood. \n" +
            "Its manifestations include hunger and malnutrition, limited access to education and other basic services, social discrimination and exclusion as well as the lack of participation in decision-making. \n" +
            "Economic growth must be inclusive to provide sustainable jobs and promote equality.\n" +
            "836 million people still live in extreme poverty \n" +
            "About one in five persons in developing regions lives on less than $1.25 per day\n" +
            "The overwhelming majority of people living on less than $1.25 a day belong to two regions: Southern Asia and sub-Saharan Africa\n" +
            "High poverty rates are often found in small, fragile and conflict-affected countries\n" +
            "One in seven children under age five in the world has inadequate height for his or her age\n" +
            "Every day in 2014, 42,000 people had to abandon their homes to seek protection due to conflict\n" +
            "By 2030, eradicate extreme poverty for all people everywhere, currently measured as people living on less than $1.25 a day\n" +
            "By 2030, reduce at least by half the proportion of men, women and children of all ages living in poverty in all its dimensions according to national definitions\n" +
            "Implement nationally appropriate social protection systems and measures for all, including floors, and by 2030 achieve substantial coverage of the poor and the vulnerable\n" +
            "By 2030, ensure that all men and women, in particular the poor and the vulnerable, have equal rights to economic resources, as well as access to basic services, ownership and control over land and other forms of 13 property, inheritance, natural resources, appropriate new technology and financial services, including microfinance\n" +
            "By 2030, build the resilience of the poor and those in vulnerable situations and reduce their exposure and vulnerability to climate-related extreme events and other economic, social and environmental shocks and disasters\n" +
            "Ensure significant mobilization of resources from a variety of sources, including through enhanced development cooperation, in order to provide adequate and predictable means for developing countries, in particular least developed countries, to implement programmes and policies to end poverty in all its dimensions\n" +
            "Create sound policy frameworks at the national, regional and international levels, based on pro-poor and gender-sensitive development strategies, to support accelerated investment in poverty eradication actions";
    public final static String sustainableDevelopmentGoal2 = "Goal 2: End hunger, achieve food security and improved nutrition and promote sustainable agriculture\n" +
            "It is time to rethink how we grow, share and consume our food.\n" +
            "If done right, agriculture, forestry and fisheries can provide nutritious food for all and generate decent incomes, while supporting people-centred rural development and protecting the environment.\n" +
            "Right now, our soils, freshwater, oceans, forests and biodiversity are being rapidly degraded. \n" +
            "Climate change is putting even more pressure on the resources we depend on, increasing risks associated with disasters such as droughts and floods. \n" +
            "Many rural women and men can no longer make ends meet on their land, forcing them to migrate to cities in search of opportunities.\n" +
            "A profound change of the global food and agriculture system is needed if we are to nourish today’s 795 million hungry and the additional 2 billion people expected by 2050.\n" +
            "The food and agriculture sector offers key solutions for development, and is central for hunger and poverty eradication.\n" +
            "Hunger\n" +
            "Globally, one in nine people in the world today (795 million) are undernourished\n" +
            "The vast majority of the world’s hungry people live in developing countries, where 12.9 per cent of the population is undernourished.\n" +
            "Asia is the continent with the most hungry people – two thirds of the total. \n" +
            "The percentage in southern Asia has fallen in recent years but in western Asia it has increased slightly.\n" +
            "Southern Asia faces the greatest hunger burden, with about 281 million undernourished people. \n" +
            "In sub-Saharan Africa, projections for the 2014-2016 period indicate a rate of undernourishment of almost 23 per cent.\n" +
            "Poor nutrition causes nearly half (45 per cent) of deaths in children under five – 3.1 million children each year.\n" +
            "One in four of the world’s children suffer stunted growth. \n" +
            "In developing countries the proportion can rise to one in three.\n" +
            "66 million primary school-age children attend classes hungry across the developing world, with 23 million in Africa alone.\n" +
            "Food security\n" +
            "Agriculture is the single largest employer in the world, providing livelihoods for 40 per cent of today’s global population. \n" +
            "It is the largest source of income and jobs for poor rural households.\n" +
            "500 million small farms worldwide, most still rainfed, provide up to 80 per cent of food consumed in a large part of the developing world. \n" +
            "Investing in smallholder women and men is an important way to increase food security and nutrition for the poorest, as well as food production for local and global markets.\n" +
            "Since the 1900s, some 75 per cent of crop diversity has been lost from farmers’ fields. \n" +
            "Better use of agricultural biodiversity can contribute to more nutritious diets, enhanced livelihoods for farming communities and more resilient and sustainable farming systems.\n" +
            "If women farmers had the same access to resources as men, the number of hungry in the world could be reduced by up to 150 million.\n" +
            "1.4 billion people have no access to electricity worldwide – most of whom live in rural areas of the developing world. \n" +
            "Energy poverty in many regions is a fundamental barrier to reducing hunger and ensuring that the world can produce enough food to meet future demand.\n" +
            "By 2030, end hunger and ensure access by all people, in particular the poor and people in vulnerable situations, including infants, to safe, nutritious and sufficient food all year round\n" +
            "By 2030, end all forms of malnutrition, including achieving, by 2025, the internationally agreed targets on stunting and wasting in children under 5 years of age, and address the nutritional needs of adolescent girls, pregnant and lactating women and older persons\n" +
            "By 2030, double the agricultural productivity and incomes of small-scale food producers, in particular women, indigenous peoples, family farmers, pastoralists and fishers, including through secure and equal access to land, other productive resources and inputs, knowledge, financial services, markets and opportunities for value addition and non-farm employment\n" +
            "By 2030, ensure sustainable food production systems and implement resilient agricultural practices that increase productivity and production, that help maintain ecosystems, that strengthen capacity for adaptation to climate change, extreme weather, drought, flooding and other disasters and that progressively improve land and soil quality\n" +
            "By 2020, maintain the genetic diversity of seeds, cultivated plants and farmed and domesticated animals and their related wild species, including through soundly managed and diversified seed and plant banks at the national, regional and international levels, and promote access to and fair and equitable sharing of benefits arising from the utilization of genetic resources and associated traditional knowledge, as internationally agreed\n" +
            "Increase investment, including through enhanced international cooperation, in rural infrastructure, agricultural research and extension services, technology development and plant and livestock gene banks in order to enhance agricultural productive capacity in developing countries, in particular least developed countries\n" +
            "Correct and prevent trade restrictions and distortions in world agricultural markets, including through the parallel elimination of all forms of agricultural export subsidies and all export measures with equivalent effect, in accordance with the mandate of the Doha Development Round\n" +
            "Adopt measures to ensure the proper functioning of food commodity markets and their derivatives and facilitate timely access to market information, including on food reserves, in order to help limit extreme food price volatility";
    public final static String sustainableDevelopmentGoal3 = "Goal 3: Ensure healthy lives and promote well-being for all at all ages\n" +
            "Ensuring healthy lives and promoting the well-being for all at all ages is essential to sustainable development. \n" +
            "Significant strides have been made in increasing life expectancy and reducing some of the common killers associated with child and maternal mortality. \n" +
            "Major progress has been made on increasing access to clean water and sanitation, reducing malaria, tuberculosis, polio and the spread of HIV/AIDS. \n" +
            "However, many more efforts are needed to fully eradicate a wide range of diseases and address many different persistent and emerging health issues.\n" +
            "Child health\n" +
            "17,000 fewer children die each day than in 1990, but more than six million children still die before their fifth birthday each year\n" +
            "Since 2000, measles vaccines have averted nearly 15.6 million deaths\n" +
            "Despite determined global progress, an increasing proportion of child deaths are in sub-Saharan Africa and Southern Asia. \n" +
            "Four out of every five deaths of children under age five occur in these regions.\n" +
            "Children born into poverty are almost twice as likely to die before the age of five as those from wealthier families.\n" +
            "Children of educated mothers—even mothers with only primary schooling—are more likely to survive than children of mothers with no education.\n" +
            "Maternal health\n" +
            "Maternal mortality has fallen by almost 50 per cent since 1990\n" +
            "In Eastern Asia, Northern Africa and Southern Asia, maternal mortality has declined by around two-thirds\n" +
            "But maternal mortality ratio – the proportion of mothers that do not survive childbirth compared to those who do –   in developing regions is still 14 times higher than in the developed regions\n" +
            "More women are receiving antenatal care. \n" +
            "In developing regions, antenatal care increased from 65 per cent in 1990 to 83 per cent in 2012\n" +
            "Only half of women in developing regions receive the recommended amount of health care they need\n" +
            "Fewer teens are having children in most developing regions, but progress has slowed. \n" +
            "The large increase in contraceptive use in the 1990s was not matched in the 2000s\n" +
            "The need for family planning is slowly being met for more women, but demand is increasing at a rapid pace\n" +
            "HIV/AIDS, malaria and other diseases\n" +
            "At the end of 2014, there were 13.6 million people accessing antiretroviral therapy\n" +
            "New HIV infections in 2013 were estimated at 2.1 million, which was 38 per cent lower than in 2001\n" +
            "At the end of 2013, there were an estimated 35 million people living with HIV\n" +
            "At the end of 2013, 240 000 children were newly infected with HIV\n" +
            "New HIV infections among children have declined by 58 per cent since 2001\n" +
            "Globally, adolescent girls and young women face gender-based inequalities, exclusion, discrimination and violence, which put them at increased risk of acquiring HIV\n" +
            "HIV is the leading cause of death for women of reproductive age worldwide\n" +
            "TB-related deaths in people living with HIV have fallen by 36% since 2004\n" +
            "There were 250 000 new HIV infections among adolescents in 2013, two thirds of which were among adolescent girls\n" +
            "AIDS is now the leading cause of death among adolescents (aged 10–19) in Africa and the second most common cause of death among adolescents globally\n" +
            "In many settings, adolescent girls’ right to privacy and bodily autonomy is not respected, as many report that their first sexual experience was forced\n" +
            "As of 2013, 2.1 million adolescents were living with HIV\n" +
            "Over 6.2 million malaria deaths have been averted between 2000 and 2015, primarily of children under five years of age in sub-Saharan Africa. \n" +
            "The global malaria incidence rate has fallen by an estimated 37 per cent and the morality rates by 58 per cent\n" +
            "Between 2000 and 2013, tuberculosis prevention, diagnosis and treatment interventions saved an estimated 37 million lives. \n" +
            "The tuberculosis mortality rate fell by 45 per cent and the prevalence rate by 41 per cent between 1990 and 2013\n" +
            "By 2030, reduce the global maternal mortality ratio to less than 70 per 100,000 live births\n" +
            "By 2030, end preventable deaths of newborns and children under 5 years of age, with all countries aiming to reduce neonatal mortality to at least as low as 12 per 1,000 live births and under-5 mortality to at least as low as 25 per 1,000 live births\n" +
            "By 2030, end the epidemics of AIDS, tuberculosis, malaria and neglected tropical diseases and combat hepatitis, water-borne diseases and other communicable diseases\n" +
            "By 2030, reduce by one third premature mortality from non-communicable diseases through prevention and treatment and promote mental health and well-being\n" +
            "Strengthen the prevention and treatment of substance abuse, including narcotic drug abuse and harmful use of alcohol\n" +
            "By 2020, halve the number of global deaths and injuries from road traffic accidents 3.7 By 2030, ensure universal access to sexual and reproductive health-care services, including for family planning, information and education, and the integration of reproductive health into national strategies and programmes\n" +
            "Achieve universal health coverage, including financial risk protection, access to quality essential health-care services and access to safe, effective, quality and affordable essential medicines and vaccines for all\n" +
            "By 2030, substantially reduce the number of deaths and illnesses from hazardous chemicals and air, water and soil pollution and contamination\n" +
            "Strengthen the implementation of the World Health Organization Framework Convention on Tobacco Control in all countries, as appropriate\n" +
            "Support the research and development of vaccines and medicines for the communicable and noncommunicable diseases that primarily affect developing countries, provide access to affordable essential medicines and vaccines, in accordance with the Doha Declaration on the TRIPS Agreement and Public Health, which affirms the right of developing countries to use to the full the provisions in the Agreement on Trade Related Aspects of Intellectual Property Rights regarding flexibilities to protect public health, and, in particular, provide access to medicines for all\n" +
            "Substantially increase health financing and the recruitment, development, training and retention of the health workforce in developing countries, especially in least developed countries and small island developing States\n" +
            "Strengthen the capacity of all countries, in particular developing countries, for early warning, risk reduction and management of national and global health risks";
    public final static String sustainableDevelopmentGoal4 = "Goal 4: Ensure inclusive and quality education for all and promote lifelong learning\n" +
            "Obtaining a quality education is the foundation to improving people’s lives and sustainable development.\n" +
            "Major progress has been made towards increasing access to education at all levels and increasing enrolment rates in schools particularly for women and girls.\n" +
            "Basic literacy skills have improved tremendously, yet bolder efforts are needed to make even greater strides for achieving universal education goals.\n" +
            "For example, the world has achieved equality in primary education between girls and boys, but few countries have achieved that target at all levels of education.\n" +
            "Enrolment in primary education in developing countries has reached 91 per cent but 57 million children remain out of school\n" +
            "More than half of children that have not enrolled in school live in sub-Saharan Africa\n" +
            "An estimated 50 per cent of out-of-school children of primary school age live in conflict-affected areas\n" +
            "103 million youth worldwide lack basic literacy skills, and more than 60 per cent of them are women\n" +
            "By 2030, ensure that all girls and boys complete free, equitable and quality primary and secondary education leading to relevant and Goal-4 effective learning outcomes\n" +
            "By 2030, ensure that all girls and boys have access to quality early childhood development, care and preprimary education so that they are ready for primary education\n" +
            "By 2030, ensure equal access for all women and men to affordable and quality technical, vocational and tertiary education, including university\n" +
            "By 2030, substantially increase the number of youth and adults who have relevant skills, including technical and vocational skills, for employment, decent jobs and entrepreneurship\n" +
            "By 2030, eliminate gender disparities in education and ensure equal access to all levels of education and vocational training for the vulnerable, including persons with disabilities, indigenous peoples and children in vulnerable situations\n" +
            "By 2030, ensure that all youth and a substantial proportion of adults, both men and women, achieve literacy and numeracy\n" +
            "By 2030, ensure that all learners acquire the knowledge and skills needed to promote sustainable development, including, among others, through education for sustainable development and sustainable lifestyles, human rights, gender equality, promotion of a culture of peace and non-violence, global citizenship and appreciation of cultural diversity and of culture’s contribution to sustainable development\n" +
            "Build and upgrade education facilities that are child, disability and gender sensitive and provide safe, nonviolent, inclusive and effective learning environments for all\n" +
            "By 2020, substantially expand globally the number of scholarships available to developing countries, in particular least developed countries, small island developing States and African countries, for enrolment in higher education, including vocational training and information and communications technology, technical, engineering and scientific programmes, in developed countries and other developing countries\n" +
            "By 2030, substantially increase the supply of qualified teachers, including through international cooperation for teacher training in developing countries, especially least developed countries and small island developing states";
    public final static String sustainableDevelopmentGoal5 = "Goal 5: Achieve gender equality and empower all women and girls\n" +
            "While the world has achieved progress towards gender equality  and women’s empowerment under the Millennium Development Goals (including equal access to primary education between girls and boys), women and girls continue to suffer discrimination and violence in every part of the world.\n" +
            "Gender equality is not only a fundamental human right, but a necessary foundation for a peaceful, prosperous and sustainable world.\n" +
            "Providing women and girls with equal access to education, health care, decent work, and representation in political and economic decision-making processes will fuel sustainable economies and benefit societies and humanity at large.\n" +
            "About  two thirds of countries in the developing regions have achieved gender parity in primary education\n" +
            "In Southern Asia, only 74 girls were enrolled in primary school for every 100 boys in 1990.\n" +
            "By 2012, the enrolment ratios were the same for girls as for boys.\n" +
            "In sub-Saharan Africa, Oceania and Western Asia, girls still face barriers to entering both primary and secondary school.\n" +
            "Women in Northern Africa hold less than one in five paid jobs in the non-agricultural sector.\n" +
            "The proportion of women in paid employment outside the agriculture sector has increased from 35 per cent in 1990 to 41 per cent in 2015\n" +
            "In 46 countries, women now hold more than 30 per cent of seats in national parliament in at least one chamber.\n" +
            "End all forms of discrimination against all women and girls everywhere\n" +
            "Eliminate all forms of violence against all women and girls in the public and private spheres, including trafficking and sexual and other types of exploitation\n" +
            "Eliminate all harmful practices, such as child, early and forced marriage and female genital mutilation\n" +
            "Recognize and value unpaid care and domestic work through the provision of public services, infrastructure and social protection policies and the promotion of shared responsibility within the household and the family as nationally appropriate\n" +
            "Ensure women’s full and effective participation and equal opportunities for leadership at all levels of decisionmaking in political, economic and public life\n" +
            "Ensure universal access to sexual and reproductive health and reproductive rights as agreed in accordance with the Programme of Action of the International Conference on Population and Development and the Beijing Platform for Action and the outcome documents of their review conferences\n" +
            "Undertake reforms to give women equal rights to economic resources, as well as access to ownership and control over land and other forms of property, financial services, inheritance and natural resources, in accordance with national laws\n" +
            "Enhance the use of enabling technology, in particular information and communications technology, to promote the empowerment of women\n" +
            "Adopt and strengthen sound policies and enforceable legislation for the promotion of gender equality and the empowerment of all women and girls at all levels";
    public final static String sustainableDevelopmentGoal6 = "Goal 6: Ensure access to water and sanitation for all\n" +
            "Clean, accessible water for all is an essential part of the world we want to live in.\n" +
            "There is sufficient fresh water on the planet to achieve this.\n" +
            "But due to bad economics or poor infrastructure, every year millions of people, most of them children, die from diseases associated with inadequate water supply, sanitation and hygiene.\n" +
            "Water scarcity, poor water quality and inadequate sanitation negatively impact food security, livelihood choices and educational opportunities for poor families across the world.\n" +
            "Drought afflicts some of the world’s poorest countries, worsening hunger and malnutrition.\n" +
            "By 2050, at least one in four people is likely to live in a country affected by chronic or recurring shortages of fresh water.\n" +
            "2.6 billion people have gained access to improved drinking water sources since 1990, but 663 million people are still without\n" +
            "At least 1.8 billion people globally use a source of drinking water that is fecally contaminated\n" +
            "Between 1990 and 2015, the proportion of the global population using an improved drinking water source has increased from 76 per cent to 91 per cent\n" +
            "But water scarcity affects more than 40 per cent of the global population and is projected to rise.\n" +
            "Over 1.7 billion people are currently living in river basins where water use exceeds recharge\n" +
            "2.4 billion people lack access to basic sanitation services, such as toilets or latrines\n" +
            "More than 80 per cent of wastewater resulting from human activities is discharged into rivers or sea without any pollution removal\n" +
            "Each day,nearly 1,000 children die due to preventable water and sanitation-related  diarrhoeal diseases\n" +
            "Hydropower is the most important and widely-used renewable source of energy and as of 2011, represented 16 per cent of total electricity production worldwide\n" +
            "Approximately 70 per cent of all water abstracted from rivers, lakes and aquifers is used for irrigation\n" +
            "Floods and other water-related disasters account for 70 per cent of all deaths related to natural disasters\n" +
            "By 2030, achieve universal and equitable access to safe and affordable drinking water for all\n" +
            "By 2030, achieve access to adequate and equitable sanitation and hygiene for all and end open defecation, paying special attention to the needs of women and girls and those in vulnerable situations\n" +
            "By 2030, improve water quality by reducing pollution, eliminating dumping and minimizing release of hazardous chemicals and materials, halving the proportion of untreated wastewater and substantially increasing recycling and safe reuse globally\n" +
            "By 2030, substantially increase water-use efficiency across all sectors and ensure sustainable withdrawals and supply of freshwater to address water scarcity and substantially reduce the number of people suffering from water scarcity\n" +
            "By 2030, implement integrated water resources management at all levels, including through transboundary cooperation as appropriate\n" +
            "By 2020, protect and restore water-related ecosystems, including mountains, forests, wetlands, rivers, aquifers and lakes\n" +
            "By 2030, expand international cooperation and capacity-building support to developing countries in water- and sanitation-related activities and programmes, including water harvesting, desalination, water efficiency, wastewater treatment, recycling and reuse technologies\n" +
            "Support and strengthen the participation of local communities in improving water and sanitation management";
    public final static String sustainableDevelopmentGoal7 = "Goal 7: Ensure access to affordable, reliable, sustainable and modern energy for all\n" +
            "Energy is central to nearly every major challenge and opportunity the world faces today.\n" +
            "Be it for jobs, security, climate change, food production or increasing incomes, access to energy for all is essential.\n" +
            "Sustainable energy is opportunity – it transforms lives, economies and the planet.\n" +
            "UN Secretary-General Ban Ki-moon is leading a Sustainable Energy for All initiative to ensure universal access to modern energy services, improve efficiency and increase use of renewable sources.\n" +
            "One in five people still lacks access to modern electricity\n" +
            "3 billion people rely on wood, coal, charcoal or animal waste for cooking and heating\n" +
            "Energy is the dominant contributor to climate change, accounting for around 60 per cent of total global greenhouse gas emissions\n" +
            "Reducing the carbon intensity of energy is a key objective in long-term climate goals.\n" +
            "By 2030, ensure universal access to affordable, reliable and modern energy services\n" +
            "By 2030, increase substantially the share of renewable energy in the global energy mix\n" +
            "By 2030, double the global rate of improvement in energy efficiency\n" +
            "By 2030, enhance international cooperation to facilitate access to clean energy research and technology, including renewable energy, energy efficiency and advanced and cleaner fossil-fuel technology, and promote investment in energy infrastructure and clean energy technology\n" +
            "By 2030, expand infrastructure and upgrade technology for supplying modern and sustainable energy services for all in developing countries, in particular least developed countries, small island developing States, and land-locked developing countries, in accordance with their respective programmes of support";
    public final static String sustainableDevelopmentGoal8 = "Goal 8: Promote inclusive and sustainable economic growth, employment and decent work for all\n" +
            "Roughly half the world’s population still lives on the equivalent of about US$2 a day.\n" +
            "And in too many places, having a job doesn’t guarantee the ability to escape from poverty.\n" +
            "This slow and uneven progress requires us to rethink and retool our economic and social policies aimed at eradicating poverty.\n" +
            "A continued lack of decent work opportunities, insufficient investments and under-consumption lead to an erosion of the basic social contract underlying democratic societies: that all must share in progress.\n" +
            "The creation of quality jobs will remain a major challenge for almost all economies well beyond 2015.\n" +
            "Sustainable economic growth will require societies to create the conditions that allow people to have quality jobs that stimulate the economy while not harming the environment.\n" +
            "Job opportunities and decent working conditions are also required for the whole working age population.\n" +
            "Global unemployment increased from 170 million in 2007 to nearly 202 million in 2012, of which about 75 million are young women and men.\n" +
            "Nearly 2.2 billion people live below the US$2 poverty line and that poverty eradication is only possible through stable and well-paid jobs.\n" +
            "470 million jobs are needed globally for new entrants to the labour market between 2016 and 2030.\n" +
            "Sustain per capita economic growth in accordance with national circumstances and, in particular, at least 7 per cent gross domestic product growth per annum in the least developed countries\n" +
            "Achieve higher levels of economic productivity through diversification, technological upgrading and innovation, including through a focus on high-value added and labour-intensive sectors\n" +
            "Promote development-oriented policies that support productive activities, decent job creation, entrepreneurship, creativity and innovation, and encourage the formalization and growth of micro-, small- and medium-sized enterprises, including through access to financial services\n" +
            "Improve progressively, through 2030, global resource efficiency in consumption and production and endeavour to decouple economic growth from environmental degradation, in accordance with the 10-year framework of programmes on sustainable consumption and production, with developed countries taking the lead\n" +
            "By 2030, achieve full and productive employment and decent work for all women and men, including for young people and persons with disabilities, and equal pay for work of equal value\n" +
            "By 2020, substantially reduce the proportion of youth not in employment, education or training\n" +
            "Take immediate and effective measures to eradicate forced labour, end modern slavery and human trafficking and secure the prohibition and elimination of the worst forms of child labour, including recruitment and use of child soldiers, and by 2025 end child labour in all its forms\n" +
            "Protect labour rights and promote safe and secure working environments for all workers, including migrant workers, in particular women migrants, and those in precarious employment\n" +
            "By 2030, devise and implement policies to promote sustainable tourism that creates jobs and promotes local culture and products\n" +
            "Strengthen the capacity of domestic financial institutions to encourage and expand access to banking, insurance and financial services for all\n" +
            "Increase Aid for Trade support for developing countries, in particular least developed countries, including through the Enhanced Integrated Framework for Trade-Related Technical Assistance to Least Developed Countries\n" +
            "By 2020, develop and operationalize a global strategy for youth employment and implement the Global Jobs Pact of the International Labour Organization";
    public final static String sustainableDevelopmentGoal9 = "Goal 9: Build resilient infrastructure, promote sustainable industrialization and foster innovation\n" +
            "Investments in infrastructure – transport, irrigation, energy and information and communication technology – are crucial to achieving sustainable development and empowering communities in many countries.\n" +
            "It has long been recognized that growth in productivity and incomes, and improvements in health and education outcomes require investment in infrastructure.\n" +
            "Inclusive and sustainable industrial development is the primary source of income generation, allows for rapid and sustained increases in living standards for all people, and provides the technological solutions to environmentally sound industrialization.\n" +
            "Technological progress is the foundation of efforts to achieve environmental objectives, such as increased resource and energy-efficiency.\n" +
            "Without technology and innovation, industrialization will not happen, and without industrialization, development will not happen.\n" +
            "Basic infrastructure like roads, information and communication technologies, sanitation, electrical power and water remains scarce in many developing countries\n" +
            "About 2.6 billion people in the developing world are facing difficulties in accessing electricity full time\n" +
            "2.5 billion people worldwide lack access to basic sanitation and almost 800 million people lack access to water, many hundreds of millions of them in Sub Saharan Africa and South Asia\n" +
            "1-1.5 million people do not have access to reliable phone services\n" +
            "Quality infrastructure is positively related to the achievement of social, economic and political goals\n" +
            "Inadequate infrastructure leads to a lack of access to markets, jobs, information and training, creating a major barrier to doing business\n" +
            "Undeveloped infrastructures limits access to health care and education\n" +
            "For many African countries, particularly the lower-income countries, the existent constraints regarding infrastructure affect firm productivity by around 40 per cent\n" +
            "Manufacturing is an important employer, accounting for around 470 million jobs worldwide in 2009 – or around 16 per cent of the world’s workforce of 2.9 billion.\n" +
            "In 2013, it is estimated that there were more than half a billion jobs in manufacturing\n" +
            "Industrialization’s job multiplication effect has a positive impact on society.\n" +
            "Every one job in manufacturing creates 2.2 jobs in other sectors\n" +
            "Small and medium-sized enterprises that engage in industrial processing and manufacturing are the most critical for the early stages of industrialization and are typically the largest job creators.\n" +
            "They make up over 90 per cent of business worldwide and account for between 50-60 per cent of employment\n" +
            "In countries where data are available, the number of people employed in renewable energy sectors is presently around 2.3 million.\n" +
            "Given the present gaps in information, this is no doubt a very conservative figure.\n" +
            "Because of strong rising interest in energy alternatives, the possible total employment for renewables by 2030 is 20 million jobs\n" +
            "Least developed countries have immense potential for industrialization in food and beverages (agro-industry), and textiles and garments, with good prospects for sustained employment generation and higher productivity\n" +
            "Middle-income countries can benefit from entering the basic and fabricated metals industries, which offer a range of products facing rapidly growing international demand\n" +
            "In developing countries, barely 30 per cent of agricultural production undergoes industrial processing.\n" +
            "In high-income countries, 98 per cent is processed.\n" +
            "This suggests that there are great opportunities for developing countries in agribusiness\n" +
            "Develop quality, reliable, sustainable and resilient infrastructure, including regional and transborder infrastructure, to support economic development and human well-being, with a focus on affordable and equitable access for all\n" +
            "Promote inclusive and sustainable industrialization and, by 2030, significantly raise industry’s share of employment and gross domestic product, in line with national circumstances, and double its share in least developed countries\n" +
            "Increase the access of small-scale industrial and other enterprises, in particular in developing countries, to financial services, including affordable credit, and their integration into value chains and markets\n" +
            "By 2030, upgrade infrastructure and retrofit industries to make them sustainable, with increased resource-use efficiency and greater adoption of clean and environmentally sound technologies and industrial processes, with all countries taking action in accordance with their respective capabilities\n" +
            "Enhance scientific research, upgrade the technological capabilities of industrial sectors in all countries, in particular developing countries, including, by 2030, encouraging innovation and substantially increasing the number of research and development workers per 1 million people and public and private research and development spending\n" +
            "Facilitate sustainable and resilient infrastructure development in developing countries through enhanced financial, technological and technical support to African countries, least developed countries, landlocked developing countries and small island developing States 18\n" +
            "Support domestic technology development, research and innovation in developing countries, including by ensuring a conducive policy environment for, inter alia, industrial diversification and value addition to commodities\n" +
            "Significantly increase access to information and communications technology and strive to provide universal and affordable access to the Internet in least developed countries by 2020";
    public final static String sustainableDevelopmentGoal10 = "Goal 10: Reduce inequality within and among countries\n" +
            "The international community has made significant strides towards lifting people out of poverty.\n" +
            "The most vulnerable nations – the least developed countries, the landlocked developing countries and the small island developing states – continue to make inroads into poverty reduction.\n" +
            "However, inequality still persists and large disparities remain in access to health and education services and other assets.\n" +
            "Additionally, while income inequality between countries may have been reduced, inequality within countries has risen.\n" +
            "There is growing consensus that economic growth is not sufﬁcient to reduce poverty if it is not inclusive and if it does not involve the three dimensions of sustainable development – economic, social and environmental.\n" +
            "To reduce inequality, policies should be universal in principle paying attention to the needs of disadvantaged and marginalized populations.\n" +
            "On average—and taking into account population size—income inequality increased by 11 per cent in developing countries between 1990 and 2010\n" +
            "A significant majority of households in developing countries—more than 75 per cent of the population—are living today in societies where income is more unequally distributed than it was in the 1990s\n" +
            "Evidence shows that, beyond a certain threshold, inequality harms growth and poverty reduction, the quality of relations in the public and political spheres and individuals’ sense of fulfilment and self-worth\n" +
            "There is nothing inevitable about growing income inequality; several countries have managed to contain or reduce income inequality while achieving strong growth performance\n" +
            "Income inequality cannot be effectively tackled unless the underlying inequality of opportunities is addressed\n" +
            "In a global survey conducted by UN Development Programme, policy makers from around the world acknowledged that inequality in their countries is generally high and potentially a threat to long-term social and economic development\n" +
            "Evidence from developing countries shows that children in the poorest 20 per cent of the populations are still up to three times more likely to die before their fifth birthday than children in the richest quintiles\n" +
            "Social protection has been significantly extended globally, yet persons with disabilities are up to five times more likely than average to incur catastrophic health expenditures\n" +
            "Despite overall declines in maternal mortality in the majority of developing countries, women in rural areas are still up to three times more likely to die while giving birth than women living in urban centres\n" +
            "By 2030, progressively achieve and sustain income growth of the bottom 40 per cent of the population at a rate higher than the national average\n" +
            "By 2030, empower and promote the social, economic and political inclusion of all, irrespective of age, sex, disability, race, ethnicity, origin, religion or economic or other status\n" +
            "Ensure equal opportunity and reduce inequalities of outcome, including by eliminating discriminatory laws, policies and practices and promoting appropriate legislation, policies and action in this regard\n" +
            "Adopt policies, especially fiscal, wage and social protection policies, and progressively achieve greater equality\n" +
            "Improve the regulation and monitoring of global financial markets and institutions and strengthen the implementation of such regulations\n" +
            "Ensure enhanced representation and voice for developing countries in decision-making in global international economic and financial institutions in order to deliver more effective, credible, accountable and legitimate institutions\n" +
            "Facilitate orderly, safe, regular and responsible migration and mobility of people, including through the implementation of planned and well-managed migration policies\n" +
            "Implement the principle of special and differential treatment for developing countries, in particular least developed countries, in accordance with World Trade Organization agreements\n" +
            "Encourage official development assistance and financial flows, including foreign direct investment, to States where the need is greatest, in particular least developed countries, African countries, small island developing States and landlocked developing countries, in accordance with their national plans and programmes\n" +
            "By 2030, reduce to less than 3 per cent the transaction costs of migrant remittances and eliminate remittance corridors with costs higher than 5 per cent";
    public final static String sustainableDevelopmentGoal11 = "Goal 11: Make cities inclusive, safe, resilient and sustainable\n" +
            "Cities are hubs for ideas, commerce, culture, science, productivity, social development and much more.\n" +
            "At their best, cities have enabled people to advance socially and economically.\n" +
            "However, many challenges exist to maintaining cities in a way that continues to create jobs and prosperity while not straining land and resources.\n" +
            "Common urban challenges include congestion, lack of funds to provide basic services, a shortage of adequate housing and declining infrastructure.\n" +
            "The challenges cities face can be overcome in ways that allow them to continue to thrive and grow, while improving resource use and reducing pollution and poverty.\n" +
            "The future we want includes cities of opportunities for all, with access to basic services, energy, housing, transportation and more.\n" +
            "Half of humanity – 3.5 billion people – lives in cities today\n" +
            "By 2030, almost 60 per cent of the world’s population will live in urban areas\n" +
            "95 per cent of urban expansion in the next decades will take place in developing world\n" +
            "828 million people live in slums today and the number keeps rising\n" +
            "The world’s cities occupy just 3 per cent of the Earth’s land, but account for 60-80 per cent of energy consumption and 75 per cent of carbon emissions\n" +
            "Rapid urbanization is exerting pressure on fresh water supplies, sewage, the living environment, and public health\n" +
            "But the high density of cities can bring efficiency gains and technological innovation while reducing resource and energy consumption\n" +
            "By 2030, ensure access for all to adequate, safe and affordable housing and basic services and upgrade slums\n" +
            "By 2030, provide access to safe, affordable, accessible and sustainable transport systems for all, improving road safety, notably by expanding public transport, with special attention to the needs of those in vulnerable situations, women, children, persons with disabilities and older persons\n" +
            "By 2030, enhance inclusive and sustainable urbanization and capacity for participatory, integrated and sustainable human settlement planning and management in all countries\n" +
            "Strengthen efforts to protect and safeguard the world’s cultural and natural heritage\n" +
            "By 2030, significantly reduce the number of deaths and the number of people affected and substantially decrease the direct economic losses relative to global gross domestic product caused by disasters, including water-related disasters, with a focus on protecting the poor and people in vulnerable situations\n" +
            "By 2030, reduce the adverse per capita environmental impact of cities, including by paying special attention to air quality and municipal and other waste management\n" +
            "By 2030, provide universal access to safe, inclusive and accessible, green and public spaces, in particular for women and children, older persons and persons with disabilities\n" +
            "Support positive economic, social and environmental links between urban, peri-urban and rural areas by strengthening national and regional development planning\n" +
            "By 2020, substantially increase the number of cities and human settlements adopting and implementing integrated policies and plans towards inclusion, resource efficiency, mitigation and adaptation to climate change, resilience to disasters, and develop and implement, in line with the Sendai Framework for Disaster Risk Reduction 2015-2030, holistic disaster risk management at all levels\n" +
            "Support least developed countries, including through financial and technical assistance, in building sustainable and resilient buildings utilizing local materials";
    public final static String sustainableDevelopmentGoal12 = "Goal 12: Ensure sustainable consumption and production patterns\n" +
            "Sustainable consumption and production is about promoting resource and energy efficiency, sustainable infrastructure, and providing access to basic services, green and decent jobs and a better quality of life for all.\n" +
            "Its implementation helps to achieve overall development plans, reduce future economic, environmental and social costs, strengthen economic competitiveness and reduce poverty.\n" +
            "Sustainable consumption and production  aims at “doing more and better with less,” increasing net welfare gains from economic activities by reducing resource use, degradation and pollution along the whole lifecycle, while increasing quality of life.\n" +
            "It involves different stakeholders, including business, consumers, policy makers, researchers, scientists, retailers, media, and development cooperation agencies, among others.\n" +
            "It also requires a systemic approach and cooperation among actors operating in the supply chain, from producer to final consumer.\n" +
            "It involves engaging consumers through awareness-raising and education on sustainable consumption and lifestyles, providing consumers with adequate information through standards and labels and engaging in sustainable public procurement, among others.\n" +
            "Each year, an estimated one third of all food produced – equivalent to 1.3 billion tonnes worth around $1 trillion – ends up rotting in the bins of consumers and retailers, or spoiling due to poor transportation and harvesting practices\n" +
            "If people worldwide switched to energy efficient lightbulbs the world would save US$120 billion annually\n" +
            "Should the global population reach 9.6 billion by 2050, the equivalent of almost three planets could be required to provide the natural resources needed to sustain current lifestyles\n" +
            "Water\n" +
            "Less than 3 per cent of the world’s water is fresh (drinkable), of which 2.5 per cent is frozen in the Antarctica, Arctic and glaciers.\n" +
            "Humanity must therefore rely on 0.5 per cent for all of man’s ecosystem’s and fresh water needs.\n" +
            "Man is polluting water faster than nature can recycle and purify water in rivers and lakes.\n" +
            "More than 1 billion people still do not have access to fresh water.\n" +
            "Excessive use of water contributes to the global water stress.\n" +
            "Water is free from nature but the infrastructure needed to deliver it is expensive.\n" +
            "Energy\n" +
            "Despite technological advances that have promoted energy efficiency gains, energy use in OECD countries will continue to grow another 35 per cent by 2020.\n" +
            "Commercial and residential energy use is the second most rapidly growing area of global energy use after transport.\n" +
            "In 2002 the motor vehicle stock in OECD countries was 550 million vehicles (75 per cent of which were personal cars).\n" +
            "A 32 per cent increase in vehicle ownership is expected by 2020.\n" +
            "At the same time, motor vehicle kilometres are projected to increase by 40 per cent and global air travel is projected to triple in the same period.\n" +
            "Households consume 29 per cent of global energy and consequently contribute to 21 per cent of resultant CO2 emissions.\n" +
            "One-fifth of the world’s final energy consumption in 2013 was from renewables.\n" +
            "Food\n" +
            "While substantial environmental impacts from food occur in the production phase (agriculture, food processing), households influence these impacts through their dietary choices and habits.\n" +
            "This consequently affects the environment through food-related energy consumption and waste generation.\n" +
            "3 billion tonnes of food is wasted every year while almost 1 billion people go undernourished and another 1 billion hungry.\n" +
            "Overconsumption of food is detrimental to our health and the environment.\n" +
            "2 billion people globally are overweight or obese.\n" +
            "Land degradation, declining soil fertility, unsustainable water use, overfishing and marine environment degradation are all lessening the ability of the natural resource base to supply food.\n" +
            "The food sector accounts for around 30 per cent of the world’s total energy consumption and accounts for around 22 per cent of total Greenhouse Gas emissions.\n" +
            "Implement the 10-year framework of programmes on sustainable consumption and production, all countries taking action, with developed countries taking the lead, taking into account the development and capabilities of developing countries\n" +
            "By 2030, achieve the sustainable management and efficient use of natural resources\n" +
            "By 2030, halve per capita global food waste at the retail and consumer levels and reduce food losses along production and supply chains, including post-harvest losses\n" +
            "By 2020, achieve the environmentally sound management of chemicals and all wastes throughout their life cycle, in accordance with agreed international frameworks, and significantly reduce their release to air, water and soil in order to minimize their adverse impacts on human health and the environment\n" +
            "By 2030, substantially reduce waste generation through prevention, reduction, recycling and reuse\n" +
            "Encourage companies, especially large and transnational companies, to adopt sustainable practices and to integrate sustainability information into their reporting cycle\n" +
            "Promote public procurement practices that are sustainable, in accordance with national policies and priorities\n" +
            "By 2030, ensure that people everywhere have the relevant information and awareness for sustainable development and lifestyles in harmony with nature\n" +
            "Support developing countries to strengthen their scientific and technological capacity to move towards more sustainable patterns of consumption and production\n" +
            "Develop and implement tools to monitor sustainable development impacts for sustainable tourism that creates jobs and promotes local culture and products\n" +
            "Rationalize inefficient fossil-fuel subsidies that encourage wasteful consumption by removing market distortions, in accordance with national circumstances, including by restructuring taxation and phasing out those harmful subsidies, where they exist, to reflect their environmental impacts, taking fully into account the specific needs and conditions of developing countries and minimizing the possible adverse impacts on their development in a manner that protects the poor and the affected communities";
    public final static String sustainableDevelopmentGoal13 = "Goal 13: Take urgent action to combat climate change and its impacts\n" +
            "Climate change is now affecting every country on every continent\n" +
            "It is disrupting national economies and affecting lives, costing people, communities and countries dearly today and even more tomorrow.\n" +
            "People are experiencing the significant impacts of climate change, which include changing weather patterns, rising sea level, and more extreme weather events.\n" +
            "The greenhouse gas emissions from human activities are driving climate change and continue to rise.\n" +
            "They are now at their highest levels in history.\n" +
            "Without action, the world’s average surface temperature is projected to rise over the 21st century and is likely to surpass 3 degrees Celsius this century—with some areas of the world expected to warm even more.\n" +
            "The poorest and most vulnerable people are being affected the most.\n" +
            "Affordable, scalable solutions are now available to enable countries to leapfrog to cleaner, more resilient economies.\n" +
            "The pace of change is quickening as more people are turning to renewable energy and a range of other measures that will reduce emissions and increase adaptation efforts.\n" +
            "But climate change is a global challenge that does not respect national borders.\n" +
            "Emissions anywhere affect people everywhere.\n" +
            "It is an issue that requires solutions that need to be coordinated at the international level and it requires international cooperation to help developing countries move toward a low-carbon economy.\n" +
            "To address climate change, countries are working to adopt a global agreement in Paris this December.\n" +
            "Thanks to the Intergovernmental Panel on Climate Change we know:\n" +
            "From 1880 to 2012, average global temperature increased by 0.85°C.\n" +
            "To put this into perspective, for each 1 degree of temperature increase, grain yields decline by about 5 per cent.\n" +
            "Maize, wheat and other major crops have experienced significant yield reductions at the global level of 40 megatonnes per year between 1981 and 2002 due to a warmer climate.\n" +
            "Oceans have warmed, the amounts of snow and ice have diminished and sea level has risen.\n" +
            "From 1901 to 2010, the global average sea level rose by 19 cm as oceans expanded due to warming and ice melted.\n" +
            "The Arctic’s sea ice extent has shrunk in every successive decade since 1979, with 1.07 million km² of ice loss every decade\n" +
            "Given current concentrations and on-going emissions of greenhouse gases, it is likely that by the end of this century, the increase in global temperature will exceed 1.5°C compared to 1850 to 1900 for all but one scenario.\n" +
            "The world’s oceans will warm and ice melt will continue.\n" +
            "Average sea level rise is predicted as 24 – 30cm by 2065 and 40-63cm by 2100.\n" +
            "Most aspects of climate change will persist for many centuries even if emissions are stopped\n" +
            "Global emissions of carbon dioxide (CO2) have increased by almost 50 per cent since 1990\n" +
            "Emissions grew more quickly between 2000 and 2010 than in each of the three previous decades\n" +
            "It is still possible, using a wide array of technological measures and changes in behaviour, to limit the increase in global mean temperature to two degrees Celsius above pre-industrial levels\n" +
            "Major institutional and technological change will give a better than even chance that global warming will not exceed this threshold\n" +
            "Strengthen resilience and adaptive capacity to climate-related hazards and natural disasters in all countries\n" +
            "Integrate climate change measures into national policies, strategies and planning\n" +
            "Improve education, awareness-raising and human and institutional capacity on climate change mitigation, adaptation, impact reduction and early warning\n" +
            "Implement the commitment undertaken by developed-country parties to the United Nations Framework Convention on Climate Change to a goal of mobilizing jointly $100 billion annually by 2020 from all sources to address the needs of developing countries in the context of meaningful mitigation actions and transparency on implementation and fully operationalize the Green Climate Fund through its capitalization as soon as possible\n" +
            "Promote mechanisms for raising capacity for effective climate change-related planning and management in least developed countries and small island developing States, including focusing on women, youth and local and marginalized communities\n" +
            "* Acknowledging that the United Nations Framework Convention on Climate Change is the primary international, intergovernmental forum for negotiating the global response to climate change.";
    public final static String sustainableDevelopmentGoal14 = "Goal 14: Conserve and sustainably use the oceans, seas and marine resources\n" +
            "The world’s oceans – their temperature, chemistry, currents and life – drive global systems that make the Earth habitable for humankind.\n" +
            "Our rainwater, drinking water, weather, climate, coastlines, much of our food, and even the oxygen in the air we breathe, are all ultimately provided and regulated by the sea.\n" +
            "Throughout history, oceans and seas have been vital conduits for trade and transportation.\n" +
            "Careful management of this essential global resource is a key feature of a sustainable future.\n" +
            "Oceans cover three quarters of the Earth’s surface, contain 97 per cent of the Earth’s water, and represent 99 per cent of the living space on the planet by volume\n" +
            "Over three billion people depend on marine and coastal biodiversity for their livelihoods\n" +
            "Globally, the market value of marine and coastal resources and industries is estimated at $3 trillion per year or about 5 per cent of global GDP\n" +
            "Oceans contain nearly 200,000 identified species, but actual numbers may lie in the millions\n" +
            "Oceans absorb about 30 per cent of carbon dioxide produced by humans, buffering the impacts of global warming\n" +
            "Oceans serve as the world’s largest source of protein, with more than 3 billion people depending on the oceans as their primary source of protein\n" +
            "Marine fisheries directly or indirectly employ over 200 million people\n" +
            "Subsidies for fishing are contributing to the rapid depletion of many fish species and are preventing efforts to save and restore global fisheries and related jobs, causing ocean fisheries to generate US$ 50 billion less per year than they could\n" +
            "As much as 40 per cent of the world oceans are heavily affected by human activities, including pollution, depleted fisheries, and loss of coastal habitats\n" +
            "By 2025, prevent and significantly reduce marine pollution of all kinds, in particular from land-based activities, including marine debris and nutrient pollution\n" +
            "By 2020, sustainably manage and protect marine and coastal ecosystems to avoid significant adverse impacts, including by strengthening their resilience, and take action for their restoration in order to achieve healthy and productive oceans\n" +
            "Minimize and address the impacts of ocean acidification, including through enhanced scientific cooperation at all levels\n" +
            "By 2020, effectively regulate harvesting and end overfishing, illegal, unreported and unregulated fishing and destructive fishing practices and implement science-based management plans, in order to restore fish stocks in the shortest time feasible, at least to levels that can produce maximum sustainable yield as determined by their biological characteristics\n" +
            "By 2020, conserve at least 10 per cent of coastal and marine areas, consistent with national and international law and based on the best available scientific information\n" +
            "By 2020, prohibit certain forms of fisheries subsidies which contribute to overcapacity and overfishing, eliminate subsidies that contribute to illegal, unreported and unregulated fishing and refrain from introducing new such subsidies, recognizing that appropriate and effective special and differential treatment for developing and least developed countries should be an integral part of the World Trade Organization fisheries subsidies negotiation\n" +
            "By 2030, increase the economic benefits to Small Island developing States and least developed countries from the sustainable use of marine resources, including through sustainable management of fisheries, aquaculture and tourism\n" +
            "Increase scientific knowledge, develop research capacity and transfer marine technology, taking into account the Intergovernmental Oceanographic Commission Criteria and Guidelines on the Transfer of Marine Technology, in order to improve ocean health and to enhance the contribution of marine biodiversity to the development of developing countries, in particular small island developing States and least developed countries\n" +
            "Provide access for small-scale artisanal fishers to marine resources and markets\n" +
            "Enhance the conservation and sustainable use of oceans and their resources by implementing international law as reflected in UNCLOS, which provides the legal framework for the conservation and sustainable use of oceans and their resources, as recalled in paragraph 158 of The Future We Want";
    public final static String sustainableDevelopmentGoal15 = "Goal 15: Sustainably manage forests, combat desertification, halt and reverse land degradation, halt biodiversity loss\n" +
            "Forests cover 30 per cent of the Earth’s surface and in addition to providing food security and shelter, forests are key to combating climate change, protecting biodiversity and the homes of the indigenous population.\n" +
            "Thirteen million hectares of forests are being lost every year while the persistent degradation of drylands has led to the desertification of 3.6 billion hectares.\n" +
            "Deforestation and desertification – caused by human activities and climate change – pose major challenges to sustainable development and have affected the lives and livelihoods of millions of people in the fight against poverty.\n" +
            "Efforts are being made to manage forests and combat desertification.\n" +
            "Forests\n" +
            "Around 1.6 billion people depend on forests for their livelihood.\n" +
            "This includes some 70 million indigenous people\n" +
            "Forests are home to more than 80 per cent of all terrestrial species of animals, plants and insects\n" +
            "Desertification\n" +
            "2.6 billion people depend directly on agriculture, but 52 per cent of the land used for agriculture is moderately or severely affected by soil degradation\n" +
            "As of 2008, land degradation affected 1.5 billion people globally\n" +
            "Arable land loss is estimated at 30 to 35 times the historical rate\n" +
            "Due to drought and desertification each year 12 million hectares are lost (23 hectares per minute), where 20 million tons of grain could have been grown\n" +
            "74 per cent of the poor are directly affected by land degradation globally\n" +
            "Biodiversity\n" +
            "Of the 8,300 animal breeds known, 8 per cent are extinct and 22 per cent are at risk of extinction\n" +
            "Of the over 80,000 tree species, less than 1 per cent have been studied for potential use\n" +
            "Fish provide 20 per cent of animal protein to about 3 billion people.\n" +
            "Only ten species provide about 30 per cent of marine capture fisheries and ten species provide about 50 per cent of aquaculture production\n" +
            "Over 80 per cent of the human diet is provided by plants.\n" +
            "Only three cereal crops – rice, maize and wheat – provide 60 per cent of energy intake\n" +
            "As many as 80 per cent of people living in rural areas in developing countries rely on traditional plant-\u00AD‐based medicines for basic\n" +
            "healthcare\n" +
            "Micro-organisms and invertebrates are key to ecosystem services, but their contributions are still poorly known and rarely acknowledged\n" +
            "By 2020, ensure the conservation, restoration and sustainable use of terrestrial and inland freshwater ecosystems and their services, in particular forests, wetlands, mountains and drylands, in line with obligations under international agreements\n" +
            "By 2020, promote the implementation of sustainable management of all types of forests, halt deforestation, restore degraded forests and substantially increase afforestation and reforestation globally\n" +
            "By 2030, combat desertification, restore degraded land and soil, including land affected by desertification, drought and floods, and strive to achieve a land degradation-neutral world\n" +
            "By 2030, ensure the conservation of mountain ecosystems, including their biodiversity, in order to enhance their capacity to provide benefits that are essential for sustainable development\n" +
            "Take urgent and significant action to reduce the degradation of natural habitats, halt the loss of biodiversity and, by 2020, protect and prevent the extinction of threatened species\n" +
            "Promote fair and equitable sharing of the benefits arising from the utilization of genetic resources and promote appropriate access to such resources, as internationally agreed\n" +
            "Take urgent action to end poaching and trafficking of protected species of flora and fauna and address both demand and supply of illegal wildlife products\n" +
            "By 2020, introduce measures to prevent the introduction and significantly reduce the impact of invasive alien species on land and water ecosystems and control or eradicate the priority species\n" +
            "By 2020, integrate ecosystem and biodiversity values into national and local planning, development processes, poverty reduction strategies and accounts\n" +
            "Mobilize and significantly increase financial resources from all sources to conserve and sustainably use biodiversity and ecosystems\n" +
            "Mobilize significant resources from all sources and at all levels to finance sustainable forest management and provide adequate incentives to developing countries to advance such management, including for conservation and reforestation\n" +
            "Enhance global support for efforts to combat poaching and trafficking of protected species, including by increasing the capacity of local communities to pursue sustainable livelihood opportunities";
    public final static String sustainableDevelopmentGoal16 = "Goal 16: Promote just, peaceful and inclusive societies\n" +
            "Goal 16 of the Sustainable Development Goals is dedicated to the promotion of peaceful and inclusive societies for sustainable development, the provision of access to justice for all, and building effective, accountable institutions at all levels.\n" +
            "Among the institutions most affected by corruption are the judiciary and police\n" +
            "Corruption, bribery, theft and tax evasion cost some US $1.26 trillion for developing countries per year; this amount of money could be used to lift those who are living on less than $1.25 a day above $1.25 for at least six years\n" +
            "The rate of children leaving primary school in conflict affected countries reached 50 per cent in 2011, which accounts to 28.5 million children, showing the impact of unstable societies on one of the major goals of the post 2015 agenda: education.\n" +
            "The rule of law and development have a significant interrelation and are mutually reinforcing, making it essential for sustainable development at the national and international level\n" +
            "Significantly reduce all forms of violence and related death rates everywhere\n" +
            "End abuse, exploitation, trafficking and all forms of violence against and torture of children\n" +
            "Promote the rule of law at the national and international levels and ensure equal access to justice for all\n" +
            "By 2030, significantly reduce illicit financial and arms flows, strengthen the recovery and return of stolen assets and combat all forms of organized crime\n" +
            "Substantially reduce corruption and bribery in all their forms\n" +
            "Develop effective, accountable and transparent institutions at all levels\n" +
            "Ensure responsive, inclusive, participatory and representative decision-making at all levels\n" +
            "Broaden and strengthen the participation of developing countries in the institutions of global governance\n" +
            "By 2030, provide legal identity for all, including birth registration\n" +
            "Ensure public access to information and protect fundamental freedoms, in accordance with national legislation and international agreements\n" +
            "Strengthen relevant national institutions, including through international cooperation, for building capacity at all levels, in particular in developing countries, to prevent violence and combat terrorism and crime\n" +
            "Promote and enforce non-discriminatory laws and policies for sustainable development";
    public final static String sustainableDevelopmentGoal17 = "Goal 17: Revitalize the global partnership for sustainable development\n" +
            "A successful sustainable development agenda requires partnerships between governments, the private sector and civil society.\n" +
            "These inclusive partnerships built upon principles and values, a shared vision, and shared goals that place people and the planet at the centre, are needed at the global, regional, national and local level.\n" +
            "Urgent action is needed to mobilize, redirect and unlock the transformative power of trillions of dollars of private resources to deliver on sustainable development objectives.\n" +
            "Long-term investments, including foreign direct investment, are needed in critical sectors, especially in developing countries.\n" +
            "These include sustainable energy, infrastructure and transport, as well as information and communications technologies.\n" +
            "The public sector will need to set a clear direction.\n" +
            "Review and monitoring frameworks, regulations and incentive structures that enable such investments must be retooled to attract investments and reinforce sustainable development.\n" +
            "National oversight mechanisms such as supreme audit institutions and oversight functions by legislatures should be strengthened.\n" +
            "Official development assistance stood at $135.2 billion in 2014, the highest level ever recorded\n" +
            "79 per cent of imports from developing countries enter developed countries duty-free\n" +
            "The debt burden on developing countries remains stable at about 3 per cent of export revenue\n" +
            "The number of Internet users in Africa almost doubled in the past four years\n" +
            "30 per cent of the world’s youth are digital natives, active online for at least five years\n" +
            "But more four billion people do not use the Internet, and 90 per cent of them are from the developing world\n" +
            "Finance\n" +
            "Strengthen domestic resource mobilization, including through international support to developing countries, to improve domestic capacity for tax and other revenue collection\n" +
            "Developed countries to implement fully their official development assistance commitments, including the commitment by many developed countries to achieve the target of 0.7 per cent of ODA/GNI to developing countries and 0.15 to 0.20 per cent of ODA/GNI to least developed countries ODA providers are encouraged to consider setting a target to provide at least 0.20 per cent of ODA/GNI to least developed countries\n" +
            "Mobilize additional financial resources for developing countries from multiple sources\n" +
            "Assist developing countries in attaining long-term debt sustainability through coordinated policies aimed at fostering debt financing, debt relief and debt restructuring, as appropriate, and address the external debt of highly indebted poor countries to reduce debt distress\n" +
            "Adopt and implement investment promotion regimes for least developed countries\n" +
            "Technology\n" +
            "Enhance North-South, South-South and triangular regional and international cooperation on and access to science, technology and innovation and enhance knowledge sharing on mutually agreed terms, including through improved coordination among existing mechanisms, in particular at the United Nations level, and through a global technology facilitation mechanism\n" +
            "Promote the development, transfer, dissemination and diffusion of environmentally sound technologies to developing countries on favourable terms, including on concessional and preferential terms, as mutually agreed\n" +
            "Fully operationalize the technology bank and science, technology and innovation capacity-building mechanism for least developed countries by 2017 and enhance the use of enabling technology, in particular information and communications technology\n" +
            "Capacity building\n" +
            "Enhance international support for implementing effective and targeted capacity-building in developing countries to support national plans to implement all the sustainable development goals, including through North-South, South-South and triangular cooperation\n" +
            "Trade\n" +
            "Promote a universal, rules-based, open, non-discriminatory and equitable multilateral trading system under the World Trade Organization, including through the conclusion of negotiations under its Doha Development Agenda\n" +
            "Significantly increase the exports of developing countries, in particular with a view to doubling the least developed countries’ share of global exports by 2020\n" +
            "Realize timely implementation of duty-free and quota-free market access on a lasting basis for all least developed countries, consistent with World Trade Organization decisions, including by ensuring that preferential rules of origin applicable to imports from least developed countries are transparent and simple, and contribute to facilitating market access\n" +
            "Systemic issues\n" +
            "Policy and institutional coherence\n" +
            "Enhance global macroeconomic stability, including through policy coordination and policy coherence\n" +
            "Enhance policy coherence for sustainable development\n" +
            "Respect each country’s policy space and leadership to establish and implement policies for poverty eradication and sustainable development\n" +
            "Multi-stakeholder partnerships\n" +
            "Enhance the global partnership for sustainable development, complemented by multi-stakeholder partnerships that mobilize and share knowledge, expertise, technology and financial resources, to support the achievement of the sustainable development goals in all countries, in particular developing countries\n" +
            "Encourage and promote effective public, public-private and civil society partnerships, building on the experience and resourcing strategies of partnerships\n" +
            "Data, monitoring and accountability\n" +
            "By 2020, enhance capacity-building support to developing countries, including for least developed countries and small island developing States, to increase significantly the availability of high-quality, timely and reliable data disaggregated by income, gender, age, race, ethnicity, migratory status, disability, geographic location and other characteristics relevant in national contexts\n" +
            "By 2030, build on existing initiatives to develop measurements of progress on sustainable development that complement gross domestic product, and support statistical capacity-building in developing countries";

}
